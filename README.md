# Thrumpenkiit #

Projet WebGL DMII 1

Graphistes : Leopoldine MENNESSIER & Eva THIVIN
Développeuses : Chloé DAVID & Elodie OUDOT

Site en ligne : http://p7598.phpnet.org/thrumpenkiit/

***

# Installation du projet #

### Installation du dépôt ###

Cloner le projet avec SourceTree ou suivre les étapes suivantes :

* Démarrer Wamp ou Mamp.
* Lancer le terminal et choisir un dossier de reception.
* Exécuter la commande "cd " + "chemin de reception" dans le dossier Wamp ou Mamp.
* Pour cloner le projet, éxécuter la commande dans le terminal "git clone lienduprojet"

### Installation du projet ###

* node.js est nécéssaire pour l'installation du projet.
* Si vous n'avez node.js voici un lien pour l'installer https://openclassrooms.com/courses/des-applications-ultra-rapides-avec-node-js/installer-node-js
* Dans votre IDE, ouvrez le dossier dans lequel vous avez cloné le projet /dev. ==> dans la suite de l'explication, nous appeleront le dossier créé thrumpenkiit
* Dans la console de votre IDE, éxecutez la commande "npm install", cette commande permet de lire le fichier "package.json" qui va contenir des modules.

### Configurer le dépôt ###

* Accédez au fichier dev/gulp/config.js pour configurer le module browserSync (il permet de rafraîchir le navigateur quand on modifie un fichier scss, js ou php)
* Sur la ligne 22 il faut modifier le proxy nommez thrumpenkiit.localhost.<br/>
* Si vous êtes sur Mac lancer Mamp cliquez sur hosts : Ajouter un host thrumpenkiit.localhost Puis indiquez le chemin du projet "thrumpenkiit/www" Vérifiez bien que vous avec bien configurer vos ports en cliquant sur Ports > Set ports to 80, 81, 443, 7443 & 3306
* Si vous êtes sur Windows lancer Wamp ajoutez un host en cliquand sur Wamp>Vos VirtualsHosts gestion VirtualHost <br/> * Suivez les mêmes étapes que Mamp
* Ne pas hésiter à relancer Wamp ou Mamp pour valider la création du Virtual Host

### Lancer le projet ###

* Dans le terminal, executez cd thrumpenkiit/dev
* Executez npm start
* Le projet devrait se lancer tout seul et lancer le navigateur, sinon accédez à http://thrumpenkiit.localhost/