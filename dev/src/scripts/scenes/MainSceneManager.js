/****************
 * Main Scene Manager
 *  This Class is an heritance of SceneManager.
 *  This Class has her own methods, like setCameraPosition, etc
 */
'use strict';
import * as THREE from 'three';

window.THREE = THREE;

// require('three/examples/js/controls/MapControls');
require('scripts/libs/threejs/Controls/MapControlsThrumpenkiit');
require('three/examples/js/loaders/ColladaLoader');

import SceneManager from "./SceneManager";
import ColladaLoad from '../utils/ColladaLoad';
import Raycaster from '../utils/Raycaster';

import {
    productView
} from '../views/ProductView';
import {
    bubbleComponent
} from '../components/BubbleComponent';
import {
    navigationManager
} from '../navigation/NavigationManager';
import {
    productManager
} from '../products/ProductManager';
import Data from '../data/Data';

export default class MainSceneManager extends SceneManager {
    constructor(canvas) {
        super(canvas);

        this.camera = this.buildCamera(this.windowDimensions);
        this.cameraTranslation = 0;
        this.controls = this.buildControls();

        this.decor = this.buildDecor();

        this.colladaLoader = new ColladaLoad(this.scene, 'assets/models/Scene.dae');
        this.colladaLoader.loadCollada("mainScene");

        this.Raycaster = new Raycaster(this.camera);
        this.raycastResult = undefined;
        this.enableRaycast = true;

        this.cursor = Data.components.cursor;

        this.sceneProducts = this.colladaLoader.sceneProducts;
        console.log(this.sceneProducts);
        this.onWindowResize();
    }

    buildControls() {
        var controls = new THREE.MapControlsThrumpenkiit(this.camera, this.renderer.domElement);
        controls.dampingFactor = 1;
        controls.minLimit = 350;
        controls.maxLimit = 4000;
        return controls;
    }

    buildCamera({
                    width,
                    height
                }) {
        const aspectRatio = width / height;
        const fieldOfView = 16;
        const nearPlane = 0.1;
        const farPlane = 100000;
        let camera = new THREE.PerspectiveCamera(fieldOfView, aspectRatio, nearPlane, farPlane);
        return camera;
    }


    buildDecor() {

        // Point Light Box 1 - Couloir

        var pointLightBox1Couloir = new THREE.PointLight( 0xffffff, 0.25, 0, 1);
        pointLightBox1Couloir.position.set(350, 800, 500);
        pointLightBox1Couloir.name = "pointLightBox1Couloir";

        this.scene.add(pointLightBox1Couloir);
        console.log(pointLightBox1Couloir);

        // Point Light Box 1 - 1

        var pointLightBox1_1 = new THREE.PointLight( 0xffffff, 0.8, 0, 1);
        pointLightBox1_1.position.set(600, 400, -300);
        pointLightBox1_1.name = "pointLightBox1_1";

        this.scene.add(pointLightBox1_1);
        console.log(pointLightBox1_1);

        // var sphereSize = 10;
        // var pointLightHelper = new THREE.PointLightHelper( pointLightBox1_1, sphereSize );
        // this.scene.add( pointLightHelper );


        // Point Light Box 1 - 2

        var pointLightBox1_2 = new THREE.PointLight( 0xffffff, 1, 0, 1);
        pointLightBox1_2.position.set(150, 400, -100);
        pointLightBox1_2.name = "pointLightBox1_2";

        this.scene.add(pointLightBox1_2);
        console.log(pointLightBox1_2);


        // Point Light Box 2 - Couloir

        var pointLightBox2Couloir = new THREE.PointLight( 0xffffff, 0.25, 0, 1);
        pointLightBox2Couloir.position.set(1500, 600, 2500);
        pointLightBox2Couloir.name = "pointLightBox2Couloir";

        this.scene.add(pointLightBox2Couloir);
        console.log(pointLightBox2Couloir);


        // Point Light Box 2 - 1

        var pointLightBox2_1 = new THREE.PointLight( 0xffffff, 0.5, 0, 1);
        pointLightBox2_1.position.set(1410, 500, -400);
        pointLightBox2_1.name = "pointLightBox2_1";

        this.scene.add(pointLightBox2_1);
        console.log(pointLightBox2_1);


        // Point Light Box 2 - 2

        var pointLightBox2_2 = new THREE.PointLight( 0xffffff, 0.5, 0, 1);
        pointLightBox2_2.position.set(1600, 400, -400);
        pointLightBox2_2.name = "pointLightBox2_2";

        this.scene.add(pointLightBox2_2);
        console.log(pointLightBox2_2);


        // Point Light Box 2 - Melania

        var pointLightBox2_melania = new THREE.PointLight( 0xffffff, 0.2, 0, 1);
        pointLightBox2_melania.position.set(1900, 250, 40);
        pointLightBox2_melania.name = "pointLightBox2_melania";

        this.scene.add(pointLightBox2_melania);
        console.log(pointLightBox2_melania);


        // Point Light Box 3 - Couloir

        var pointLightBox3Couloir = new THREE.PointLight( 0xffffff,  0.25, 0, 1);
        pointLightBox3Couloir.position.set(3000, 400, 2500);
        pointLightBox3Couloir.name = "pointLightBox3Couloir";

        this.scene.add(pointLightBox3Couloir);
        console.log(pointLightBox3Couloir);


        // Point Light Box 3 - 1

        var pointLightBox3_1 = new THREE.PointLight( 0xffffff, 0.7, 0, 1);
        pointLightBox3_1.position.set(2700, 400, -256);
        pointLightBox3_1.name = "pointLightBox3_1";

        this.scene.add(pointLightBox3_1);
        console.log(pointLightBox3_1);


        // Point Light Box 4 - Couloir

        var pointLightBox4Couloir = new THREE.PointLight( 0xffffff,  0.5, 0, 1);
        pointLightBox4Couloir.position.set(4500, 800, 2500);
        pointLightBox4Couloir.name = "pointLightBox4Couloir";

        this.scene.add(pointLightBox4Couloir);
        console.log(pointLightBox4Couloir);


        // Point Light Box 4 - 1

        var pointLightBox4_1 = new THREE.PointLight( 0xffffff, 1, 0, 1);
        pointLightBox4_1.position.set(4020, 300, 100);
        pointLightBox4_1.name = "pointLightBox4_1";


        this.scene.add(pointLightBox4_1);
        console.log(pointLightBox4_1);


        // SpotLight Poutine

        var spotLightPoutine = new THREE.SpotLight(0xffffff, 0.5, 0, Math.PI / 15, 1);
        spotLightPoutine.position.set(1320, 800, 500);
        spotLightPoutine.name = "spotLightPoutine";
        this.scene.add(spotLightPoutine);

        var targetObjectTrump = new THREE.Object3D();
        targetObjectTrump.position.set(1320, 0, 500);
        targetObjectTrump.name = "targetObjectTrump";
        this.scene.add(targetObjectTrump);
        spotLightPoutine.target = targetObjectTrump;


        // SpotLight Michelle

        var spotLightMichelle = new THREE.SpotLight(0xffffff, 0.5, 0, Math.PI / 15, 1);
        spotLightMichelle.position.set(1320, 800, 500);
        spotLightMichelle.name = "spotLightMichelle";
        this.scene.add(spotLightMichelle);

        var targetObjectObama = new THREE.Object3D();
        targetObjectObama.position.set(1320, 0, 500);
        targetObjectObama.name = "targetObjectTrump";
        this.scene.add(targetObjectObama);
        spotLightMichelle.target.position.copy(targetObjectObama.position);
        console.log(targetObjectObama.position);

        // var helper2 = new THREE.CameraHelper( spotLightMichelle.shadow.camera );
        // this.scene.add( helper2 );

        // var spotLightHelper = new THREE.SpotLightHelper( spotLightMichelle );
        // this.scene.add( spotLightHelper );

        console.log(this.scene);

    }

    moveCamera(vector) {
        this.camera.position.x = vector.x;
    }

    // Update function

    updateControls() {
        this.controls.update();
    }

    updateCameraPosition() {
        if (this.cameraTranslation === 1 && !(this.camera.position.x > 4000)) {
            this.camera.translateX(10);
        } else if (this.cameraTranslation === -1 && !(this.camera.position.x < 350)) {
            this.camera.translateX(-10);
        }
    }

    update() {
        const elapsedTime = this.clock.getElapsedTime();
        this.renderer.render(this.scene, this.camera);
        this.updateControls();
        this.updateCameraPosition();
    }

    // OnEvent functions

    onMouseMove(event) {
        this.Raycaster.beginRayCaster = true;
        this.mouseX = (event.clientX / window.innerWidth) * 2 - 1;
        this.mouseY = -(event.clientY / window.innerHeight) * 2 + 1;
        bubbleComponent.setPosition(event.clientX, event.clientY);
        this.cursor.style.top = event.clientY - 30 + "px";
        this.cursor.style.left = event.clientX + "px";
        if (this.Raycaster.enable === true) {

            this.Raycaster.updateMousePosition(this.mouseX, this.mouseY);
            var raycastResult = this.Raycaster.raycast(this.sceneProducts);
            if (raycastResult) {
                if (raycastResult === this.raycastResult) {
                } else {
                    this.raycastResult = raycastResult;
                    if (raycastResult) {
                        productManager.getCurrentProduct(raycastResult.myID);
                        bubbleComponent.setInfos();
                        bubbleComponent.show();
                        this.cursor.classList.add("active");
                    } else {
                        bubbleComponent.hide();
                        this.cursor.classList.remove("active");
                    }
                }
            } else {
                this.raycastResult = undefined;
                productManager.currentProduct = undefined;
                bubbleComponent.setInfos();
                bubbleComponent.hide();
                this.cursor.classList.remove("active");
            }

        }

    }

    onClick(event) {

        if (this.Raycaster.enable === true) {

            this.Raycaster.updateMousePosition(this.mouseX, this.mouseY);
            var raycastResult = this.Raycaster.raycast(this.sceneProducts);

            if (raycastResult) {
                productManager.getCurrentProduct(raycastResult.myID);
                navigationManager.showProductView();
                productView.sceneManager.onWindowResize();
            }

        }

        if (event.target.classList.contains('scrollArrow')) {
            bubbleComponent.hide();
            if (event.target.id === "scrollRight") {
                this.cameraTranslation = 1;
            } else {
                this.cameraTranslation = -1;
            }
        }

    }

    onMouseUp(){
        this.cameraTranslation = 0;
    }

    onMouseOver(event){
        if (event.target.classList.contains('scrollArrow')) {
            this.Raycaster.enable = false

        }
    }

    onMouseOut(event){
        if (event.target.classList.contains('scrollArrow')) {
            this.Raycaster.enable = true

        }
    }

    onKeydown(keyCode) {
        switch (keyCode) {
            case 37: // Left
                this.cameraTranslation = -1;
                break;
            case 39: // Right
                this.cameraTranslation = 1;
                break;
        }
    }

    onKeyup(keyCode) {
        this.cameraTranslation = 0;
    }

    onWheel(event) {
        if (event.wheelDelta > 0 && event.wheelDeltaY > event.wheelDeltaX && !(this.camera.position.x > 4000)) { // down -> right
            this.camera.translateX(10);
        } else if (event.wheelDelta < 0 && event.wheelDeltaY < event.wheelDeltaX && !(this.camera.position.x < 350)) { // up -> left
            this.camera.translateX(-10);
        } else {
            this.cameraTranslation = 0;
        }
    }
}

export {
    MainSceneManager
}
