/****************
 * Scene Manager
 *  This Class manage the creation of a scene, with camera, renderer, etc.
 *  This Class has her own methods, like setCameraPosition, etc
 */

'use strict';
import * as THREE from 'three';

window.THREE = THREE;

export default class SceneManager {

    constructor(canvas) {
        this.canvas = canvas;
        this.clock = new THREE.Clock();

        this.windowDimensions = {
            width: canvas.width,
            height: canvas.height
        };

        this.mouseX = null;
        this.mouseY = null;

        this.scene = this.buildScene();
        this.renderer = this.buildRender(this.windowDimensions);
        this.camera = this.buildCamera(this.windowDimensions);

        this.scene.add(this.camera);

    }

    // Build functions

    buildScene() {
        const scene = new THREE.Scene();
        return scene;
    }

    buildRender({
                    width,
                    height
                }) {
        const renderer = new THREE.WebGLRenderer({
            antialias: true,
            alpha: true
            // physicallyCorrectLights : true
        });
        const DPR = (window.devicePixelRatio) ? window.devicePixelRatio : 1;
        // renderer.setPixelRatio(DPR);
        renderer.setSize(width, height);
        renderer.shadowMap.enabled = true;
        renderer.shadowMap.type = THREE.PCFSoftShadowMap;
        renderer.setScissor(0, 0, width, height);
        renderer.setScissorTest = true;

        return renderer;
    }

    buildCamera({
                    width,
                    height
                }) {
        const aspectRatio = width / height;
        const fieldOfView = 20;
        const nearPlane = 0.1;
        const farPlane = 4500;
        const camera = new THREE.PerspectiveCamera(fieldOfView, aspectRatio, nearPlane, farPlane);
        return camera;
    }

    buildDecor() {


    }

    // Set functions

    setCameraPosition(x, y, z) {
        this.camera.position.set(x, y, z);
    }

    setCameraLookAt(x, y, z) {
        this.camera.lookAt(x, y, z)
    }

    onWindowResize() {
        const {
            width,
            height
        } = canvas;

        this.windowDimensions.width = width;
        this.windowDimensions.height = window.innerHeight;

        this.camera.aspect = width / height;
        this.camera.updateProjectionMatrix();

        this.renderer.setSize(width, height);
    }


}

export {
    SceneManager
}