'use strict';
import * as THREE from 'three';
window.THREE = THREE;
require('three/examples/js/controls/TransformControls');
require('three/examples/js/loaders/ColladaLoader');
import {
    productManager
} from '../products/ProductManager';
import ColladaLoadProduct from '../utils/ColladaLoadProduct';
require('three/examples/js/loaders/ColladaLoader');

import SceneManager from "./SceneManager";


export default class SecondarySceneManager extends SceneManager {
    constructor(canvas) {
        super(canvas);
        this.onWindowResize();
        this.controls = this.buildControls();

        this.decor = this.buildDecor();

    }

    buildControls() {
        var controls = new THREE.TransformControls(this.camera, this.renderer.domElement);
        controls.setMode('rotate');
        controls.setSize(10);
        controls.children[0].visible = false;
        this.scene.add(controls);
        return controls;
    }

    buildDecor() {

        // Point Light 1 - Couloir

        var pointLightProduct_1 = new THREE.PointLight( 0xffffff, 1, 0, 1);
        pointLightProduct_1.position.set(0, 0, 200);
        pointLightProduct_1.name = "pointLightProduct_1";

        this.scene.add(pointLightProduct_1);
        console.log(pointLightProduct_1);

        // Point Light 2 - Couloir

        var pointLigthProduct_2 = new THREE.PointLight( 0xffffff, 1, 0, 1);
        pointLigthProduct_2.position.set(0, 500, 0);
        pointLigthProduct_2.name = "pointLightProduct_2";

        this.scene.add(pointLigthProduct_2);
        console.log(pointLigthProduct_2);

    }

    onWindowResize() {

        this.windowDimensions.width = window.innerWidth / 2.5;
        this.windowDimensions.height = window.innerHeight;

        this.camera.aspect = this.windowDimensions.width / this.windowDimensions.height;
        this.camera.updateProjectionMatrix();

        this.renderer.setSize(this.windowDimensions.width, this.windowDimensions.height);
    }

    update() {
        const elapsedTime = this.clock.getElapsedTime();
        this.renderer.render(this.scene, this.camera);
    }

    cleanScene() {
        var object = this.scene.children[4];
        // console.log("to delete ", object);
        if(this.scene.children.length >= 5){
            // console.log('je delete', object);
            this.scene.remove(object);
        }
        // console.log(this.scene,"after delete");
        
    }

    addToScene() {
        this.scene.add(productManager.currentProduct.loadedModel);
        productManager.currentProduct.loadedModel.position.set(0, 0, 0);
        this.controls.attach(productManager.currentProduct.loadedModel);
    }

    loadToScene(scale) {
        var colladaLoader = new ColladaLoadProduct(this.scene, productManager.currentProduct.model);
        colladaLoader.loadCollada(this.controls);
    }
}

export {
    SecondarySceneManager
}
