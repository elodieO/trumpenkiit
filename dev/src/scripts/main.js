/****************
 * Main script
 */

import ProjectManager from './ProjectManager';
import $ from 'jquery';
// import gifplayer from 'gifplayer';

// Create a projectManager
const projectManager = new ProjectManager();

$('.gifplayer').on("mouseenter", function(){
    var img = $(this).find('img');
    img[0].attributes[0].value = img[0].attributes[3].value;
});

$('#buttonSpread').on("mouseenter", function(){
    $(this).addClass("gifPlusSize");
});

$('#buttonSpread').on("mouseleave", function(){
    $(this).removeClass("gifPlusSize");
});

$('.gifplayer').on("mouseleave", function(){
    var img = $(this).find('img');
    img[0].attributes[0].value = img[0].attributes[2].value;
});

$('.intro-button').on('click', function(){
    $('body > .intro').removeClass('show');
    $('body > #canvas').addClass('show');
    $('body > .menu').addClass('show');
});



