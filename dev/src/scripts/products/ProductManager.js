/****************
 * Product Manager
 *  This Class allows to create Product instances and get the current Product when it's needed
 */
'use strict';
import {
    navigationManager
} from '../navigation/NavigationManager';
import Data from '../data/Data';
import Product from '../products/Product';

class ProductManager {
    
    constructor() {
        this.currentProduct;
        this.products = [];

        this.createProductsInstances();

    }

    createProductsInstances() {
        for (var index in Data.products) {
            var product = new Product(Data.products[index]);
            this.products.push(product);
        }
    }

    getCurrentProduct(id) {

        for (let index = 0; index < this.products.length; index++) {

            if (this.products[index].id == id) {
                var currentProduct = this.products[index];
            }

        }

        if (currentProduct) {
            this.currentProduct = currentProduct;
        } else {
            this.currentProduct = undefined;
        }


    }

    getProductInfo(id){
        for (let index = 0; index < this.products.length; index++) {
            
            if (this.products[index].id == id){
                var product = this.products[index];
                return product;
            }

        }
        
    }

}

export let productManager = new ProductManager();