/****************
 * Product
 *  This Class is for product creation, stock informations, models, etc
 */
'use strict';

export default class Product {

    constructor(subject) {
        this.id = subject.id;
        this.name = subject.name;
        this.reference = subject.reference;
        this.image = subject.image;
        this.intro = subject.intro;
        this.description = subject.description;
        this.tips = subject.tips;
        this.knowledge = subject.knowledge;
        this.related = subject.related;
        this.mainColor = subject.mainColor;
        this.accentColor = subject.accentColor;
        this.category = subject.categories;
        this.lettre = subject.lettre;
        this.lettreCatalogue = subject.lettre_catalogue;
        this.composition = subject.composition;
        this.spreadable = subject.spreadable;
        this.scale = subject.scale;

        // this.model is a url to collada model
        this.model = subject.model;

        // First, individual model is not loaded
        this.alreadyLoaded = false;
        this.loadedModel;

        this.spread = false;
    }

    onSpread(element) {
        var infos = element.querySelector('.singleProductView_body-infos');
        var compo = element.querySelector('.singleProductView_body-compo');


        if (this.spread === true) {
            infos.classList.add('show');
            compo.classList.remove('show');
            if(this.spreadable === 1){
                this.loadedModel.children.forEach(function (child) {
                    child.position.set(child.originalPos.x, child.originalPos.y, child.originalPos.z);
                });
            }
            
        } else {
            compo.classList.add('show');
            infos.classList.remove('show');
            if(this.spreadable === 1){
                this.loadedModel.children.forEach(function (child) {
                    child.translateOnAxis(child.originalPos, 0.5);
                });
            }
        }

        this.spread = !this.spread;
    }
    
}

export {
    Product
}