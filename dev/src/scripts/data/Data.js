/****************
 * Data
 *  Data informations are accessible from all Classes
 */

export default {
    products: {
        0: {
            id: 0,
            name: "MÏCHELL",
            image: "images/meubles/Michelle_2_0023.png",
            model: "assets/models/products/michelle.dae",
            reference: "RENAISSANCE",
            intro: "Offrez à votre intérieur une source de lumière élégante et puissante !",
            description: "Le lampadaire en métal cuivré MÏCHELL fait rayonner n'importe quel objet se trouvant à son niveau.",
            tips: "MÏCHELL rejette tous les produits trop gras, trop salés et trop sucrés. Ce lampadaire encourage un nettoyage à base de produits sains.",
            knowledge: "MÏCHELL n'est pas là pour faire de la figuration dans votre intérieur. Elle prend les choses en main et impose son style.",
            categories : 7,
            mainColor: "#cc7e58",
            accentColor: "#9c532f",
            lettre : "images/lettres_detail/lettre_michelle.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_michelle.svg",
            spreadable:1,
            scale: 1,
            composition: {
                0: {
                    number: "1",
                    intitule: "abat-jour en cuivre",
                    image: "images/pieces/Michelle0024.png"
                },
                1: {
                    number: "1",
                    intitule: "pied en marbre",
                    image: "images/pieces/michelle_pied.png"
                },
                2: {
                    number: "1",
                    intitule: "ampoule CHÄLEUUR",
                    image: "images/pieces/michelle_ampoule.png"
                }
            },
            related: {
                0: {
                    id: 1
                },
                1: {
                    id: 2
                },
                2: {
                    id: 20
                }
            }

            
        },
        1: {
            
            id: 1,
            name: "OBMÄ",
            image: "images/meubles/Obama0023.png",
            model: "assets/models/products/obama.dae",
            reference: "RENEGADE1108",
            intro: "Donnez du caractère à votre bureau avec le fauteuil OBÄMÄ !",
            description: "Il trouvera aisément sa place sa place chez vous car il convient à tous les styles. Son assise en cuir souple et authentique est soutenue par de solides pieds qui ne fléchissent pas sous le poids de la menace. Il va aussi vous procurer un grand confort et une sécurité pour tous.",
            tips: "Son cuir ne vieillit et ne s'abime pas. Ce modèle est indémodable !",
            knowledge: "Il est très rare de trouver des fauteuils de cette teinte.",
            categories : 7,
            mainColor: "#9c705a",
            accentColor: "#6b4331",
            lettre : "images/lettres_detail/lettre_obama.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_obama.svg",
            scale: 0.5,
            spreadable:1,
            composition: {
                0: {
                    number: "2",
                    intitule: "coussins confortables",
                    image: "images/pieces/obama-assise.png"
                },
                1: {
                    number: "2",
                    intitule: "accoudoirs en acier",
                    image: "images/pieces/obama-accoudoir.png"
                }
            },
            related: {
                0: {
                    id: 0
                },
                1: {
                    id: 2
                },
                2: {
                    id: 20
                }
            }

        },
        2: {
            id: 2,
            name: "BIDEN",
            image: "images/meubles/Biden0023.png",
            model: "assets/models/products/biden.dae",
            reference: "BROMANCE",
            intro: "Le coussin BÏDËN, l'inséparable du fauteuil OBÄMÄ.",
            description: "Ces deux articles sont vendus ensemble depuis 8 ans car ils forment la paire parfaite.",
            tips: "Vous n'avez pas à vous en faire ! Sa laine venue d'Irlande est très robuste !",
            knowledge: "BÏDEN a obtenu le label \"MEDAL OF FREEDOM\" en Janvier 2017.",
            categories : 7,
            mainColor: "#8b8c88",
            accentColor: "#6b6c69",
            lettre : "images/lettres_detail/lettre_biden.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_biden.svg",
            spreadable:2,
            scale: 2,
            composition: {
                0: {
                    number: "1",
                    intitule: "housse en laine IRISH",
                    image: "images/pieces/biden.png"
                }
            },
            related: {
                0: {
                    id: 1
                },
                1: {
                    id: 0
                },
                2: {
                    id: 20
                }
            }
        },
        3: {
            id: 3,
            name: "KÜSHNËR",
            image: "images/meubles/Kushner.png",
            model: "assets/models/products/Kushner.dae",
            reference: "LEB0FIIÏS",
            intro: "Une pièce phare de notre collection enfants !",
            description: "Très bien assortis avec nos deux tabourets THRÜMP et JÜNIÖR, ces ballons raviront les plus jeunes comme les plus vieux !",
            tips: "Soyez prudents, ne tentez en aucun cas de percer les ballons car les secrets - et le sucre - qu’ils renferment pourraient bien éclater au grand jour…",
            knowledge: "Ces joyeux ballons peuvent contenir des traces de sucre provenant des bonbons russes NATÄLIARIBO. Comment ce sucre est il arrivé jusqu’ici ? Mystère...",
            categories : 2,
            mainColor: "#ffa74c",
            accentColor: "#c5710e",
            lettre : "images/lettres_detail/lettre_kushner.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_kushner.svg",
            spreadable:1,
            scale: 0.5,
            composition: {
                0: {
                    number: "1",
                    intitule: "ballon SUGARDOGGY",
                    image: "images/pieces/kushner_chien0021.png"
                },
                1: {
                    number: "3",
                    intitule: "ballons SWEËT",
                    image: "images/pieces/kushner_ballon_jaune0021.png"
                }
            },
            related: {
                0: {
                  id: 22
                },
                1: {
                  id: 18
                },
                2: {
                    id: 5
                }
            }
        },
        4: {
            id: 4,
            name: "MAÄNAFORT",
            image: "images/meubles/Pence.png",
            model: "assets/models/products/Pence.dae",
            reference: "1APRILJOKE",
            intro: "Les 10 secrets d'une ingérence russe réussie",
            description: "Un best seller au rayon des livres politiques ! Il vous démontrera l'étendue des compétences des hackers russes et traitera aussi de l'évasion fiscale. Ce livre a eu un grand succès aux Etats Unis, mais aussi en Ukraine ou encore aux Phillipines.",
            tips: "Ce livre est consulté si souvent qu'il ne prendra jamais la poussière.",
            knowledge: "Ce guide est vendu d'occasion car il est déjà passé entre de nombreuses mains.",
            categories : 0,
            mainColor: "#f6d600",
            accentColor: "#ce9e1d",
            lettre : "images/lettres_detail/lettre_pence.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_pence.svg",
            spreadable:3,
            scale: 1,
            composition: {
            },
            related: {
                0: {
                    id: 21
                }
            }
        },
        5: {
            id: 5,
            name: "JUNÏIOR",
            image: "images/meubles/TrumpJr0112.png",
            model: "assets/models/products/Trump_Jr.dae",
            reference: "D0N_MOUNTA1N33R",
            intro: "Petit tabouret deviendra grand !",
            description: "Même si il n'est pas aussi imposant (ni aussi orange) que le tabouret THRÜMP, il a une grande influence sur celui ci, et lui permet de briller davantage.",
            tips: "Un peu de vodka d'origine Russe lui donnera davantage d'éclat.",
            knowledge: "Il peut aussi être utilisé comme repose pieds en complément du tabouret THRÜMP.",
            categories : 2,
            mainColor: "#e4e823",
            accentColor: "#babf09",
            lettre : "images/lettres_detail/lettre_trump_JR.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_trump_JR.svg",
            spreadable:1,
            scale: 1,
            composition: {
                0: {
                    number: "1",
                    intitule: "assise",
                    image: "images/pieces/trumpJR-assise.png"
                },
                1: {
                    number: "4",
                    intitule: "mini-pieds bancals",
                    image: "images/pieces/trumpJR-pied.png"
                },
                2: {
                    number: "6",
                    intitule: "vices LOVEMYDAÄDDY",
                    image: "images/pieces/vis.png"
                },
                3: {
                    number: "6",
                    intitule: "vices MONËY",
                    image: "images/pieces/vis.png"
                },
                4: {
                    number: "6",
                    intitule: "vices KILLANIMÄLS",
                    image: "images/pieces/vis.png"
                }
            },
            related: {
                0: {
                    id: 22
                },
                1: {
                    id: 18
                },
                2: {
                    id: 3
                }
            }
        },
        6: {
            id: 6,
            name: "COMEY",
            image: "images/meubles/Comey.png",
            model: "assets/models/products/Comey.dae",
            reference: "",
            intro: "Il n'y a pas d'introduction pour ce meuble",
            description: "Ce meuble n'a pas de description",
            tips: "Aucune astuce d'entretien",
            knowledge: "Pas de fait particulier à connaître",
            categories : 6,
            mainColor: "#066531",
            accentColor: "#003e19",
            lettre : "images/lettres_detail/lettre_comey.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_comey.svg",
            spreadable:3,
            scale: 0.3,
            composition: {
                0: {
                    number: "Undefined",
                    intitule: "Undefined"
                }
            },
            related: {
                // 0: {
                //     id: 0
                // },
                // 1: {
                //     id: 2
                // }
            }
        },
        7: {
            id: 7,
            name: "FLŸN",
            image: "images/meubles/Flynn.png",
            model: "assets/models/products/Flynn.dae",
            reference: "",
            intro: "La bougie au parfum de défaite.",
            description: "Elle éclaire son entourage avec une certaine intensité, mais elle a tendance à se consumer assez vite. ",
            tips: "La bougie FLŸN a du mal à garder sa forme et sa couleur intactes au fil des années.",
            knowledge: "Mieux vaut tenir FLŸN éloignée du tapis HILÄRY car son tissu serait sensible aux flammes de FLŸN.",
            categories : 3,
            mainColor: "#da71be",
            accentColor: "#b7388f",
            lettre : "images/lettres_detail/lettre_flynn.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_flynn.svg",
            spreadable:2,
            scale: 0.1,
            composition: {
                0: {
                    number: "1",
                    intitule: "bougie en cire",
                    image: "images/pieces/flynn.png"

                },
                1: {
                    number: "1",
                    intitule: "ficelle usée",
                    image: "images/pieces/flynn_fil.png"

                }
            },
            related: {
                0: {
                    id: 14
                }
            }
            
        },
        8: {
            id: 8,
            name: "FÄCE B.",
            image: "images/meubles/facebook0024.png",
            model: "assets/models/products/facebook.dae",
            reference: "ANALYTICA",
            intro: "Méfiez-vous des apparences !",
            description: "Cette lettre peut paraître uniquement décorative au premier abord, mais elle possède un certain pouvoir. Sa teinte bleutée est connue pour influencer la perception de notre environnement et des objets alentours.",
            tips: "Vaporiser quotidiennement une bonne dose de notre produit ESPRIÏKRITIK sur FÄCE B. permet de limiter ses effets négatifs.",
            knowledge: "Cet objet n'existe pas en russie mais il est très convoité dans ce pays.",
            categories : 4,
            mainColor: "#6680df",
            accentColor: "#4a5da2",
            lettre : "images/lettres_detail/lettre_facebook.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_facebook.svg",
            spreadable:3,
            scale: 0.5,
            composition: {
            },
            related: {
                0: {
                    id: 11
                },
                1: {
                    id: 9
                }
            }
        },
        9: {
            id: 9,
            name: "ANÄLYTICÄ",
            image: "images/meubles/Analytica.png",
            model: "assets/models/products/analytica.dae",
            reference: "ZUCKTHER0B00T",
            intro: "Ne sous-estimez pas l’impact de ce petit objet !",
            description: "Présent dans la plupart des foyers américains, sa couleur peut changer et influencer votre humeur de manière significative.",
            tips: "Tout comme FÄCE B., un peu de notre solution ESPRIÏKRITIK permettra d’atténuer les effets d’ANÄLYTICÄ.",
            knowledge: "Certaines de ses branches seraient fabriquées en Russie, mais nous ne pouvons pas vous le certifier.",
            categories : 4,
            mainColor: "#df4444",
            accentColor: "#8f2c2c",
            lettre : "images/lettres_detail/lettres_analytica.svg",
            lettre_catalogue:"images/lettres_catalogue/lettres_analytica.svg",
            spreadable:3,
            scale: 1,
            composition: {
            
            },
            related: {
                0: {
                    id: 11
                },
                1: {
                    id: 8
                }
            }
        },
        10: {
            id: 10,
            name: "RETWËET",
            image: "images/meubles/twitter0023.png",
            model: "assets/models/products/twitter.dae",
            reference: "JDORSEY",
            intro: "La cage pour vos TWÏTTÖS chanteurs.",
            description: "Cette cage a été spécialement conçues pour les TWÏTTOS, petits volatiles furtifs et très rapides. En effet il ne faut pas les laisser dans la nature car ils sont aussi très voraces et risqueraient d'attaquer les plus faibles. De plus leur chant est très assourdissant et vient résonner dans les têtes de quiconque l'entend.",
            tips: "Laissez la cage à l'extérieur, fermez la bien derrière vous et veillez à ce que les TWÏTTÖS ne rentrent pas chez vous. Ces oiseaux sont des manipulateurs...",
            knowledge: "Malgré des barreaux en acier, les TWÏTTOS sont malins et arrivent à s'échapper. En 2016, 50 000 TWÏTTÖS se sont échappés aux Etats-Unis et ont tout ravagé sur leur passage.",
            categories : 4,
            mainColor: "#6ab0e8",
            accentColor: "#46759a",
            lettre : "images/lettres_detail/lettre_twitter.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_twitter.svg",
            spreadable:1,
            scale: 0.5,
            composition: {
                0: {
                    number: "1",
                    intitule: "cage à barreaux très fins",
                    image: "images/pieces/twitter_cage.png"
                },
                1: {
                    number: "1",
                    intitule: "balançoire PROTHRÜMP",
                    image: "images/pieces/twitter_balancoire.png"
                },
                2: {
                    number: "50 000",
                    intitule: "TWÏTTÖS",
                    image: "images/pieces/twitter_oiseau.png"
                }
            },
            related: {
                0: {
                    id: 12
                }
            }
        },
        11: {
            id: 11,
            name: "WIKI-LIÏKS",
            image: "images/meubles/Wikileaks.png",
            model: "assets/models/products/Wikileaks.dae",
            reference: "ZUCKTHER0B00T",
            intro: "Ne sous-estimez pas l’impact de ce petit objet !",
            description: "Lorsqu’on fouille dans une corbeille à papier, on peut toujours trouver des informations intéressantes… Mais dans notre corbeille WIKI-LIÏKS, on peut même dénicher des documents top-secrets  !",
            tips: "Cette corbeille se remplit vite, nous vous invitons donc à la vider régulièrement pour ne pas vous encombrer l’esprit par ce trop plein d’informations.",
            knowledge: "Ne soyez pas surpris si vos propres documents confidentiels disparaissent : des personnes peu scrupuleuses sont capables de les récupérer et les jeter dans WIKI-LIÏKS.",
            categories : 4,
            mainColor: "#39826e",
            accentColor: "#205b4b",
            lettre : "images/lettres_detail/lettre_wikileaks.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_wikileaks.svg",
            spreadable:3,
            scale: 0.5,
            composition: {
                
            },
            related: {
                0: {
                    id: 8
                },
                1: {
                    id: 9
                }
            }
        },
        12: {
            id: 12,
            name: "PEÜPLË",
            image: "images/meubles/Peuple.png",
            model: "assets/models/products/peuple.dae",
            reference: "328 103 285",
            intro: "Diviser pour mieux régner ! Les tables gigognes qui n'ont pas d'autre choix que d'aller par trois.",
            description: "Ces trois tables s'assemblent au premier abord à merveille mais sont en réalité très différentes. Elles n'ont cependant pas d'autre choix que de coexister.",
            tips: "Les tables PEÜPLE ne savent pas vraiment ce qu'il leur faut pour être entretenues. Leurs besoins sont trop différents et nous avons du mal à trouver un produit qui aille aux trois. Placer des napperons GRÄN ËLEKTEUR permet de contourner ce problème.",
            knowledge: "La moyenne table INDECÏÏ est facilement modulable et influençable.",
            categories : 4,
            mainColor: "#b96063",
            accentColor: "#8c2d33",
            lettre : "images/lettres_detail/lettre_peuple.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_peuple.svg",
            spreadable:1,
            scale: 0.5,
            composition: {
                0: {
                    number: "1",
                    intitule: "moyen plateau en bois INDECÏÏ",
                    image: "images/pieces/peuple_bois.png"
                },
                1: {
                    number: "1",
                    intitule: "petit plateau bleu DEMÖCRÄT",
                    image: "images/pieces/peuple_bleu.png"
                },
                2: {
                    number: "1",
                    intitule: "grand plateau rose PROTHRÜMP",
                    image: "images/pieces/peuple_rose.png"
                },
                3: {
                    number: "3",
                    intitule: "pieds en bois INDECÏÏ",
                    image: "images/pieces/peuple_piedgold.png"
                },
                4: {
                    number: "3",
                    intitule: "pieds bleu DEMÖCRÄT",
                    image: "images/pieces/peuple_piedbleu.png"
                },
                5: {
                    number: "3",
                    intitule: "pieds rose PROTHRÜM",
                    image: "images/pieces/peuple_piedrose.png"
                },
                6: {
                    number: "3",
                    intitule: "napperons GRÄN ËLEKTEÜR",
                    image: "images/pieces/peuple_piedrose.png"
                }
            },
            related: {
                0: {
                    id: 10
                }
            }
            

        },
        13: {
            id: 13,
            name: "MÜELLËR",
            image: "images/meubles/mueller.png",
            model: "assets/models/products/Mueller.dae",
            reference: "VIETNAM68",
            intro: "MÜELLËR remet les pendules à l'heure !",
            description: "Cette horloge murale massive s'impose dans votre salon au détriment des autres meubles. Gare à ceux qui défient le temps, MÜELLËR ne laisse rien passer et règle tout le monde à la même heure. Vous ne pourrez pas y échapper l'horloge MÜELLËR est intransigeante !",
            tips: "MÜELLËR est incorruptible, il rejette tout produit d'entretien qui tenterait de lui graisser la patte.",
            knowledge: "Le tabouret THRÜMP s'abime plus vite quand il est dans la même pièce que MÜELLËR.",
            categories : 6,
            mainColor: "#cc7e58",
            accentColor: "#9c532f",
            lettre : "images/lettres_detail/lettre_mueller.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_mueller.svg",
            spreadable:1,
            scale: 0.5,
            composition: {
                0: {
                    number: "1",
                    intitule: "cadran très résistant",
                    image: "images/pieces/mueller_cadran.png"
                },
                1: {
                    number: "1",
                    intitule: "grande aiguille JUSTÏC",
                    image: "images/pieces/mueller_grd_aiguille.png"
                },
                2: {
                    number: "1",
                    intitule: "petite aiguille ÖRD",
                    image: "images/pieces/mueller_petite_aiguille.png"
                }
            },
            related: {
                0: {
                    id: 6
                }

            }
            
        },
        14: {
            id: 14,
            name: "CÖHËN",
            image: "images/meubles/Cohen.png",
            model: "assets/models/products/Cohen.dae",
            reference: "PITBULL",
            intro: "CÖHËN, un espace de rangement idéal pour dissimuler vos secrets !",
            description: "La desserte CÖHËN offre un espace de rangement supplémentaire à votre intérieur. Grâce à son tiroir, vous pourrez garder secret vos films pornographiques de l'actrice Stormy Daniels et de Karen McDougal. Attention néanmoins à ce qu'elle ne roule pas vers une autre pièce révélant ainsi vos secrets honteux à tous !",
            tips: "Munie de roulettes, la desserte CÖHËN est facilement manipulable. Vous pouvez ainsi passer d'une pièce à l'autre non sans égarer un ou deux secret en route.",
            knowledge: "Issue de la collection de meubles COVFËFË, CÖHËN se marie aujourd'hui mieux avec la collection JUSTÏSS qui lui fournit de nombreux avantages.",
            categories : 3,
            mainColor: "#504f4a",
            accentColor: "#31302d",
            lettre : "images/lettres_detail/lettre_cohen.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_cohen.svg",
            spreadable:1,
            scale: 1,
            composition: {
                0: {
                    number: "2",
                    intitule: "roues",
                    image: "images/pieces/Cohen_roue.png"
                },
                1: {
                    number: "1",
                    intitule: "tiroir SËCRËT",
                    image: "images/pieces/Cohen_tirroir.png"
                },
                2: {
                    number: "1",
                    intitule: " étagère",
                    image: "images/pieces/Cohen_etagere.png"
                }
            },
            related: {
                0: {
                    id: 7
                }
            }
            
        },
        15: {
            id: 15,
            name: "MËLANÏA",
            image: "images/meubles/Melania.png",
            model: "assets/models/products/melania.dae",
            reference: "SL0V3NI3",
            intro: "Même si il n'y a rien à l'intérieur, il reste un très bel objet de décoration.",
            description: "Ce magnifique vase de Slovénie va ajouter de la brillance à votre pièce.  Soyez tranquille, MËLANÏA n'a aucune influence sur son environnement. Il habille à merveille chaque espace grâce à ses nombreuses parures dorées. Pour les plus audacieux, le MËLANÏA peut s'accorder avec le tabouret THRÜMP :  Excentricité garantie !",
            tips: "Faire briller avec un torchon tous les jours afin qu'il garde son éclat.",
            knowledge: "Il ne s'agit en réalité que d'une copie car l'originale a disparu.",
            categories : 8,
            mainColor: "#a5caf8",
            accentColor: "#5a82c5",
            lettre : "images/lettres_detail/lettre_melania.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_melania.svg",
            spreadable:2,
            scale: 3,
            composition: {
                0: {
                    number: "1",
                    intitule: "courbes généreuses",
                    image: "images/pieces/melania_vase.png"
                },
                1: {
                    number: "10",
                    intitule: "anneaux dorés",
                    image: "images/pieces/melania_anneau_3.png"
                }
            },
            related: {
                0: {
                    id: 17
                }
            }
            

        },
        16: {
            id: 16,
            name: "HÏLLÄRY",
            image: "images/meubles/Hillary2_0023.png",
            model: "assets/models/products/hillary.dae",
            reference: "3V3RGR33N",
            intro: "Un tapis peu imposant mais agréable pour les pieds !",
            description: "HILLARY a été spécialement conçue pour les femmes avec un intérieur classique, sobre et sans prise de risque. Son motif pied de poule était censé ravir la gente féminine mais ce fut un échec. Ce modèle n'a pas su se démarquer suffisamment et a donc été peu vendu. ",
            tips: "HÏLLÄRY prend très rapidement la poussière. Elle se fait piétiner constamment et a du mal à garder sa forme originale.",
            knowledge: "Le tapis HÏLLÄRY prend sa revanche et revient avec un nouveau modèle pour l'année 2020.",
            categories : 5,
            mainColor: "#cc8c86",
            accentColor: "#b76460",
            lettre : "images/lettres_detail/lettre_hillary.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_hillary.svg",
            spreadable:3,
            scale: 1.5,
            composition: {
            },
            related: {
                0: {
                    id: 19
                }
            }
            

        },
        17: {
            id: 17,
            name: "BERNÏE",
            image: "images/meubles/Sanders0023.png",
            model: "assets/models/products/sanders.dae",
            reference: "W33DKIPPÄ",
            intro: "Cette belle plante préfère rester très à gauche.",
            description: "Elle aime se tenir isolée des autres meubles de son environnement. Il faut surtout l'éloigner du tabouret THRÜMP, dont le plastique serait néfaste pour cette plante.",
            tips: "Vous n'avez aucun souci à vous faire, BERNÏE consomme très peu d'eau. En revanche, veillez à lui apporter des nutriments 100% biologiques.",
            knowledge: "Attention, les vapeurs qui s'en dégagent peuvent avoir des effets quelque peu… inattendus !",
            categories : 8,
            mainColor: "#a6c77e",
            accentColor: "#628834",
            lettre : "images/lettres_detail/lettre_sanders.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_sanders.svg",
            spreadable:1,
            scale: 0.5,
            composition: {
                0: {
                    number: "4",
                    intitule: "feuilles WEED",
                    image: "images/pieces/Sanders0021.png"
                },
                1: {
                    number: "3",
                    intitule: "feuilles OCB",
                    image: "images/pieces/Sanders0021.png"
                },
                2: {
                    number: "1",
                    intitule: "pot issu du commerce équitable",
                    image: "images/pieces/bernie-pot0024.png"
                }
            },
            related: {
                0: {
                    id: 15
                }
            }
            

        },
        18: {
            id: 18,
            name: "NÄTÄLIARÏBO",
            image: "images/meubles/Natalia0042.png",
            model: "assets/models/products/Natalia.dae",
            reference: "Ната́лья",
            intro: "La gourmandise est un vilain défaut !",
            description: "Les plus gourmands vont tomber dans le piège de ces sucreries russes. En même temps, comment résister à cette odeur sucrée si alléchante…",
            tips: "Attention aux tâches de sucre sur le tapis HILLARY ! Les bonbons NATALIARIBO ont une fâcheuse tendance à laisser des traces après leur passage.",
            knowledge: "On a retrouvé de nombreux bonbons NATALIARIBO dans la Trump Tower planqués sous le tabouret TRUMP JR.",
            categories : 2,
            mainColor: "#ff9090",
            accentColor: "#d45052",
            lettre : "images/lettres_detail/lettre_natalia.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_natalia.svg",
            spreadable:2,
            scale: 0.5,
            composition: {
                0: {
                    number: "1",
                    intitule: "emballage",
                    image: "images/pieces/natalia.png"
                },
                1: {
                    number: "6",
                    intitule: "bonbons KRËMLÏN",
                    image: "images/pieces/natalia_jaune.png"
                },
                2: {
                    number: "6",
                    intitule: "bonbons MÄNIGÄNCE",
                    image: "images/pieces/natalia_orange.png"
                },
                3: {
                    number: "6",
                    intitule: "bonbons TENTÄTIÖN",
                    image: "images/pieces/natalia_rose.png"
                }

            },
            related: {
                0: {
                    id: 5
                },
                1: {
                    id: 22
                },
                2: {
                    id: 3
                }
            }
            

        },
        19: {
            id: 19,
            name: "BÏLLÏ",
            image: "images/meubles/Bill.png",
            model: "assets/models/products/Bill.dae",
            reference: "EAGLE93",
            intro: "BÏLLÏ est aujourd'hui un objet de collection.",
            description: "Il fut autrefois un tabouret très populaire dans les foyers américains. Il est aujourd'hui exposé aux murs et n'a plus vraiment d'utilité.",
            tips: "Astiquez-le avec du spray \"Lewinsky\" et il retrouvera son allure d'antan.",
            knowledge: "BÏLLÏ et HÏLLARY font partis de la même collection VIKTIIM. Pourtant, BÏLLÏ a eu nettement plus de succès qu'HILLARY. Serait-ce à cause de ça que l'on ne les voit plus exposés ensemble ?",
            categories : 5,
            mainColor: "#0040a5",
            accentColor: "#002056",
            lettre : "images/lettres_detail/lettre_bill.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_bill.svg",
            spreadable:2,
            scale: 1,
            composition: {
                0: {
                    number: "1",
                    intitule: "cadre en bois",
                    image: "images/pieces/bill0021.png"
                },
                1: {
                    number: "1",
                    intitule: "tabouret bleu DEMOCRÄT",
                    image: "images/pieces/BILL.png"
                }
            },
            related: {
                0: {
                    id: 16
                }
            }
            

        },
        20: {
            id: 20,
            name: "BUSH",
            image: "images/meubles/Bush.png",
            model: "assets/models/products/bush.dae",
            reference: "TIMBERWOLF",
            intro: "Le tabouret  qui revient à la mode !",
            description: "Longtemps critiqué pour son look désuet,  BÜSH redevient un meuble d'intérieur tendance. C'est sur que quand on voit le tabouret THRÜMP,  le look de BÜSH est pas si mal !",
            tips: "Entretien traditionnel. Dépoussiérer de temps en temps. Utiliser des produits MADE IN USA de préférence.",
            knowledge: "Contre toute attente, le BUSH se marie plutôt bien avec le lampadaire MICHEL",
            categories : 7,
            mainColor: "#b16f29",
            accentColor: "#824a05",
            lettre : "images/lettres_detail/lettre_bush.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_bush.svg",
            spreadable:2,
            scale: 1,
            composition: {
                0: {
                    number: "1",
                    intitule: "tabouret MADE IN USA",
                    image: "images/pieces/Bush.png"

                },
                1: {
                    number: "1",
                    intitule: "cadre MADE IN USA",
                    image: "images/pieces/bill0021.png"
                }
            },
            related: {
                0: {
                    id: 0
                },
                1: {
                    id: 1
                },
                2: {
                    id: 2
                }
            }
            

        },
        21:{
            id: 21,
            name: "THRÜMP",
            image: "images/meubles/Trump0112.png",
            model: "assets/models/products/trump.dae",
            reference: "MOGUL0117",
            intro: "C'est aujourd'hui un basique de notre collection COVFËFË",
            description: "On ne vous le présente plus, on le voit partout, c'est le tabouret THRÜMP ! Son plastique orange très reconnaissable se marie très bien avec un intérieur 100% made in USA. Évitez donc de le placer dans une pièce trop exotique.",
            tips: "Ce tabouret a besoin de l'éclairage du lustre PÜTIN. C'est pourquoi il est préférable de le placer sous ce dernier sans quoi il risque de s'abîmer ou même de disparaître.",
            knowledge: "Le plastique utilisé est le même pour le biberons, les couches jetables et les boîtes à usage alimentaire.",
            categories : 0,
            mainColor: "#ffc457",
            accentColor: "#ca8c04",
            lettre : "images/lettres_detail/lettre_trump.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_trump.svg",
            spreadable:1,
            scale: 0.5,
            composition: {
                0: {
                    number: "1",
                    intitule: "assise en plastique MADE IN RUSSIA",
                    image: "images/pieces/trump-assise.png"
                },
                1: {
                    number: "4",
                    intitule: "pieds bancals",
                    image: "images/pieces/trump-pied.png"
                },
                2: {
                    number: "6",
                    intitule: "vices PÖWEER",
                    image: "images/pieces/vis.png"
                },
                3: {
                    number: "6",
                    intitule: "vices GÖLD",
                    image: "images/pieces/vis.png"
                },
                4: {
                    number: "6",
                    intitule: "vices SËXX",
                    image: "images/pieces/vis.png"
                }
            },
            related: {
                0: {
                    id: 4
                }
            }
        },
        22:{
            id: 22,
            name: "PÜTIINE",
            image: "images/meubles/Poutine0023.png",
            model: "assets/models/products/poutine.dae",
            reference: "KGB1998FSB",
            intro: "La vedette russe de votre salon !",
            description: "Ce lustre est imposant et majestueux. Grâce à sa grande influence, il peut changer la couleur de vos meubles selon vos envies. Ses ampoules ont été spécialement conçues pour s'immiscer dans chaque recoin de votre salon pour ne rien laisser secret.",
            tips: "Pas besoin d'entretien, ce lustre est autonome et indépendant.",
            knowledge: "Éviter au maximum de l'éteindre car il pourrait décider de vous plonger dans le noir à tout jamais.",
            categories : 2,
            mainColor: "#b70b0b",
            accentColor: "#6d0303",
            lettre : "images/lettres_detail/lettre_poutine.svg",
            lettre_catalogue:"images/lettres_catalogue/lettre_poutine.svg",
            spreadable:1,
            scale: 0.2,
            composition: {
                0: {
                    number: "1",
                    intitule: "support en or",
                    image: "images/pieces/putin_socle.png"
                },
                1: {
                    number: "12",
                    intitule: "ampoules Fancy Bear",
                    image: "images/pieces/putin_ampoule.png"
                },
                2: {
                    number: "12",
                    intitule: "ampoules Cozy Bear39",
                    image: "images/pieces/putin_ampoule_petit.png"
                }
            },
            related: {
                0: {
                    id: 5
                },
                1: {
                    id: 18
                },
                2: {
                    id: 3
                }
            }
        }
    },
    categories : {
        100: {
            id : 100,
            name : "les emblématiques",
            description : "La crème de la crème, toutes collections confondues !",
            containSub : false,
            isSub : false,
            products : {
                0: 21,
                1: 22,
                2: 17,
                3: 1,
                4: 10
            }
        },
        0 : {
            id : 0,
            name : "COVFEFË",
            description : "Bien que le nom de cette collection reste mystérieux, celle ci désigne avant tout l'intemporel tabouret en plastique THRÜMP mais aussi le célèbre guide MAÄNAFORT. Au centre de toutes les discussions, les objets COVFEFË ne laissent personne indifférent : on aime ou on déteste !",
            containSub : false,
            isSub : false
        },
        1 : {
            id : 1,
            name : "MAANIGÄNCES",
            description : "Collection MAANIGÄNCES",
            containSub : true,
            isSub : false,
            subCategories : {
                0 : {
                    id : 2,
                    name : "RUÜSSES",
                    description : "En provenance de Moscou et de Kalingrad mais aussi de New York ou encore du New Jersey, ces meubles à l'apparence si différente peuvent tout à fait être disposés dans une même pièce, aux côtés de la collection COVFEFË.",
                    containSub : false,
                    isSub : true
                },
                1 : {
                    id : 3,
                    name : "OLDIÏES",
                    description : "Oups ! Il semblerait que les OLDÏIES soient aujourd'hui quelque peu désuets. Mais, autrefois, leur aspect avait un vrai impact sur leur environnement.",
                    containSub : false,
                    isSub : true
                },
                2 : {
                    id : 4,
                    name : "MAÄNIPULÉS",
                    description : "Ces objets sont américains mais certaines de leurs pièces ont pu être modifiées en Russie. Leur atout : ils peuvent être manipulés à votre guise !",
                    containSub : false,
                    isSub : true
                }
            }
        },
        2 : {
            id : 5,
            name : "VIKTIÏM",
            description : "Ces objets peuvent vite se retrouver écrasés sous le poids d'autres meubles plus imposants.",
            containSub : false,
            isSub : false
        },
        3 : {
            id : 6,
            name : "JUSTÏSS",
            description : "Ces éléments robustes ne se brisent pas facilement et donnent à votre intérieur une certaine rigueur.",
            containSub : false,
            isSub : false
        },
        4 : {
            id : 7,
            name : "NON-THRÜMP",
            description : "Ces meubles ne s'accordent pas, mais alors pas du tout avec la collection THRÜMP… Cela dit… Le résultat d'une telle association pourrait être intéressant...",
            containSub : false,
            isSub : false
        },
        5 : {
            id : 8,
            name : "DECÖOR",
            description : "Pas d'inquiétude ! Ces décorations assez classiques n'auront aucune incidence sur l'harmonie de votre pièce.",
            containSub : false,
            isSub : false
        }
    },
    canvas:{
        mainCanvas: document.getElementById("canvas"),
        secondaryCanvas: document.getElementById("singleProductView_canvas")
    },
    views:{
        productView: document.getElementById("productView"),
        catalogView : document.getElementById('catalogView'),
        aboutView : document.getElementById('aboutView'),
        helpView : document.getElementById('helpView')
    },
    buttons : {
        closeButtons : {
            productViewButtonClose: document.getElementById("closeProductView"),
            catalogViewButtonClose : document.getElementById("closeCatalogView"),
            helpViewButtonClose : document.getElementById("closeHelpView"),
            aboutViewButtonClose : document.getElementById("closeAboutView")
        },
        catalogButton : document.getElementById("catalogButton"),
        aboutButton : document.getElementById("aboutButton"),
        helpButton : document.getElementById("helpButton"),
        productViewButtonSpread: document.getElementById("buttonSpread"),
        productViewButtonSeeOnScene: document.getElementById("seeOnScene")
    },
    components : {
        productBubble: document.getElementById("bubbleComponent"),
        cursor: document.getElementById("cursor"),
        catalogCategories: document.getElementsByClassName("categories"),
        catalogContent: document.getElementsByClassName("contents")
    },
    state: 0
};