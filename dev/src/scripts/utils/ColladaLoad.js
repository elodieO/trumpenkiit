/****************
 * Collada Load
 *  This Class allow the load of a model
 *  This Class is called by MainSceneManager
 */
'use strict';
import * as THREE from 'three';
import {
    Group
} from '../libs/threejs/three.r97';

window.THREE = THREE;
require('three/examples/js/loaders/ColladaLoader');


export default class ColladaLoad {

    constructor(scene, model) {
        this.sceneCollada = new THREE.Group();
        this.model = model;
        this.scene = scene;
        this.sceneProducts = [];
        this.loadingManager = this.buildLoadingManager();
        this.loader = this.buildLoader();
    }

    buildLoadingManager() {
        var progressPage = document.body.querySelector(".loadingPage");
        var progressBar = document.body.querySelector("#beginProgressBar");
        const loadingManager = new THREE.LoadingManager(function () {

            this.scene.add(this.sceneCollada);
            this.sceneCollada.position.set(0, 0, 0);
            this.sceneCollada.scale.set(1, 1, 1);

            this.sceneCollada.traverse(function (node) {

                node.castShadow = true;
                node.receiveShadow = true;

                if (node.material) {
                    node.material.side = THREE.DoubleSide;
                    // node.material.transparent = true;
                    node.material.shadowSide = THREE.DoubleSide;
                    // node.material.depthTest = true;

                    if (node.material.name === "oiseaux_twitter") {
                        node.material.transparent = true;
                    }

                    if (node.material.name === "plans_transparents") {
                        node.castShadow = false;
                        node.receiveShadow = false;
                    }

                    if (node.name === "abat_jour" ||
                        node.name === "arche_argent" ||
                        node.name === "arche_dore" ||
                        node.name === "VASE" ||
                        node.name === "anneaux" ||
                        node.name === "Structure_metallique" ||
                        node.name === "Cylindre.3" ||
                        node.name === "barreaux" ||
                        node.name === "poignee" ||
                        node.name === "Cambridge_Analytica" ||
                        node.uuid === "9C6C2FDE-08FF-4F65-95C9-672E618816F4"
                    ) {
                        // console.log(node);
                        node.material.reflectivity = 0;
                        node.material.metalness = 0;
                        node.material.shininess = 0;
                    }
                    if (node.name === "Bush" || node.name === "Bill_clinton") {
                        node.material.depthFunc = THREE.GreaterDepth
                    }

                }

            });

        }.bind(this));

        loadingManager.onProgress = function (item, loaded, total) {

            progressBar.style.width = (loaded / total * 100) + '%';
            if (loaded / total * 100 === 100) {
                setTimeout(() => {
                    progressPage.classList.add('fadeOut');
                }, 1000);

            }

        };

        return loadingManager;
    }

    buildLoader() {
        let loader = new THREE.ColladaLoader(this.loadingManager);
        return loader;
    }

    loadCollada() {

        this.loader.load(this.model, function (collada) {

            this.sceneCollada = collada.scene;

            var sectionsNumber = this.sceneCollada.children[0].children.length;
            var myIdIndex = 0;
            /*
             * Check for products and stock them in this.sceneProducts list
             */
            for (let sectionIndex = 0; sectionIndex < sectionsNumber; sectionIndex++) {
                const section = this.sceneCollada.children[0].children[sectionIndex];
                for (let objectIndex = 0; objectIndex < section.children[0].children.length; objectIndex++) {
                    var object = section.children[0].children[objectIndex];
                    object.myID = myIdIndex;
                    object.isParent = true;
                    object.castShadow = true;
                    if (object.children[0]) {
                        if (object.children[0].material) {

                            if (object.children[0].material.name === "plans_transparents" || object.children[0].material.name === "") {
                                // console.log(object);
                                object.children[0].material.opacity = 0;
                                object.children[0].material.transparent = true;
                            }
                        }
                    }

                    this.sceneProducts.push(object);
                    myIdIndex++;
                }

            }


        }.bind(this));
    }

}

export {
    ColladaLoad
}