/****************
 * Raycaster
 *  This Class allows to create ray and get raycast response
 */
'use strict';
import * as THREE from '../libs/threejs/three.r97';

export default class Raycaster {

    constructor(camera) {
        this.vector;
        this.mouseX = 0;
        this.mouseY = 0;
        this.camera = camera;
        this.beginRayCaster = false;
        this.enable = true;
        this.intersected = undefined;
    }

    updateMousePosition(mouseX, mouseY) {
        this.mouseX = mouseX;
        this.mouseY = mouseY;
    }

    raycast(objectsList) {

        this.vector = new THREE.Vector3(this.mouseX, this.mouseY, 1);
        this.vector.unproject(this.camera);
        var ray = new THREE.Raycaster(this.camera.position, this.vector.sub(this.camera.position).normalize());

        var intersects = ray.intersectObjects(objectsList, true);

        if (intersects.length > 0) {

            if (this.beginRayCaster != false) {

                if (intersects[0].object != this.intersected) {
                    var par = intersects[0].object;
                    // While intersects is not the global parent, continue to get the parent of intersects
                    while (par.isParent !== true) {
                        par = par.parent;
                    }
                    this.intersected = par;

                    return this.intersected;

                } else {
                    return undefined;
                }

            }

        } else {
            if (this.intersected) {

                this.intersected = null;
                return this.intersected;

            }

        }

    }

}

export {
    Raycaster
}