/****************
 * Collada Load Product
 *  This Class allows to load a product individual model
 *  This Class is called by SecondarySceneManager
 */
'use strict';
import * as THREE from 'three';
import {
    Group
} from '../libs/threejs/three.r97';
window.THREE = THREE;
require('three/examples/js/loaders/ColladaLoader');
import {
    productManager
} from '../products/ProductManager';


export default class ColladaLoadProduct {

    constructor(scene, model) {
        this.sceneCollada = new THREE.Group();
        this.model = model;
        this.scene = scene;
        this.sceneProducts = [];
        this.loadingManager = this.buildLoadingManager();
        this.loader = this.buildLoader();
    }

    buildLoadingManager() {
        const loadingManager = new THREE.LoadingManager(function () {

            this.scene.add(this.sceneCollada);
            this.sceneCollada.position.set(0, 0, 0);
            this.sceneCollada.scale.set(productManager.currentProduct.scale, productManager.currentProduct.scale, productManager.currentProduct.scale);

            this.sceneCollada.traverse(function (node) {
                if (node.material) {
                    node.material.side = THREE.DoubleSide;
                    node.material.transparent = true;
                }
            });

        }.bind(this));

        return loadingManager;
    }

    buildLoader() {
        let loader = new THREE.ColladaLoader(this.loadingManager);
        return loader;
    }

    loadCollada(controls) {

        this.loader.load(this.model, function (collada) {

            this.sceneCollada = collada.scene.children[0];

            /*
             * Foreach model children, register the initialPosition, to allow spread and reset
             */
            this.sceneCollada.children.forEach(child => {
                var originalPos = new THREE.Vector3();
                originalPos.copy(child.position);
                child.originalPos = originalPos;
            });

            /*
             * When the model is loaded for the firstTime, notify the productManager.currentProduct (the product actually show in productView) that the model is loaded and stock it
             */
            productManager.currentProduct.alreadyLoaded = true;
            productManager.currentProduct.loadedModel = this.sceneCollada;
            controls.attach(this.sceneCollada);


        }.bind(this));
    }

}

export {
    ColladaLoadProduct
}