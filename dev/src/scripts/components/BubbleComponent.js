/****************
 * Bubble Component
 *  This Component manage the bubble which show the product name on hover in globalView scene
 *  bubbleComponent is a Singleton of BubbleComponent accesible from all other Classes
 */

'use strict';
import Data from '../data/Data';
import {
    productManager
} from '../products/ProductManager';

class BubbleComponent {

    constructor() {
        this.element = Data.components.productBubble;
        this.name = "Product name";
        this.description = "Product description";
    }

    setInfos() {

        if (productManager.currentProduct) {
            this.name = productManager.currentProduct.name;
            this.description = productManager.currentProduct.intro;
            this.setName();
            this.setDescription();
        } else {
            this.name = "Undefined";
            this.description = "Undefined";
            this.setName();
            this.setDescription();
        }
    }

    setName() {
        var name = this.element.querySelector(".name");
        name.innerHTML = this.name;
    }

    setDescription(){
        var description = this.element.querySelector(".description");
        description.innerHTML = this.description;
    }

    show() {
        this.element.classList.add("show");
    }

    hide() {
        this.element.classList.remove("show");
    }

    setPosition(eventX, eventY) {
        this.element.style.top = eventY - 50 + "px";
        this.element.style.left = eventX + 50 + "px";
    }

}

export let bubbleComponent = new BubbleComponent();