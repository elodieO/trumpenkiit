/****************
 * Project Manager
 *  This Class manage the project status to update only the current scene
 *  status 0 : globalView scene is active
 *  status 1 : productView scene is active
 *  status 2 : catalogView scene is active
 */

'use strict';
import {
    productManager
} from './products/ProductManager';
import {
    globalView
} from './views/GlobalView';
import {
    productView
} from './views/ProductView';
import {
    catalogView
} from './views/CatalogView';
import {
    aboutView
} from './views/AboutView';
import {
    helpView
} from './views/HelpView';
import {
    navigationManager
} from './navigation/NavigationManager';

export default class ProjectManager {

    constructor() {
        this.state = navigationManager.state;
        this.update();
    }

    /*
     * Update function
     * This function is called on requestAnimationFrame
     * navigationManager.status is checked to update this.state
     * if this.state = 0 → update globalView scene
     * if this.state = 1 → update productView scene
     */
    update() {
        requestAnimationFrame(() => this.update());
        this.state = navigationManager.state;
        switch (this.state) {
            case 0:
                globalView.update();
                break;
            case 1:
                productView.update();
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            default:
                break;
        }
    }

}

export {
    ProjectManager
}