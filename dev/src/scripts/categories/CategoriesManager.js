/****************
 *  Categories Manager
 *  This Class allows to create Category instances and get the current Category when it's needed
 */
'use strict';
import {
    navigationManager
} from '../navigation/NavigationManager';
import Data from '../data/Data';
import Category from '../categories/Category';

class CategoriesManager {

    constructor() {
        this.currentCategory = 100;
        this.categories = [];
        this.categoryBar = document.body.querySelector(".category[data-category='"+ this.currentCategory + "']");
        this.categoryContent;
        this.categoryBar.addEventListener("click", () => this.switchCategories(event));
        this.createCategoriesInstances();
        console.log(this);
    }

    createCategoriesInstances() {
        for (var index in Data.categories) {
            var category = new Category(Data.categories[index]);
            this.categories.push(category);
            if(Data.categories[index].subCategories){
                for (var j in Data.categories[index].subCategories) {
                    var category = new Category(Data.categories[index].subCategories[j]);
                    this.categories.push(category);
                }                
            }
            
        }
    }

    switchCategories(event){
        
        var target = event.target;
        if(event.target.tagName == "P"){
            target = event.target.parentElement;
        }
    
        if(target.classList.contains("category-parent")){
        }else{
            this.categoryBar.classList.remove('active');
            this.categoryContent = document.body.querySelector(".content[data-category='"+ this.currentCategory + "']");
            this.categoryContent.classList.remove('active');
            this.currentCategory = target.attributes[1].value;
            this.categoryBar = document.body.querySelector(".category[data-category='"+ this.currentCategory + "']");
            this.categoryContent = document.body.querySelector(".content[data-category='"+ this.currentCategory + "']");
            this.categoryBar.classList.add('active');
            this.categoryContent.classList.add('active');
        }
        
    }

    getCategory(id) {

        for (let index = 0; index < this.categories.length; index++) {

            if (this.categories[index].id == id) {
                var currentCategory = this.categories[index];
            }

        }

        if(currentCategory){
            return currentCategory;
        }        

    }

}

export let categoriesManager = new CategoriesManager();