/****************
 * Category
 *  This Category is for category creation, stock informations and products that are in the category, etc
 */
'use strict';

import Data from '../data/Data';

export default class Category {

    constructor(subject) {
        this.id = subject.id;
        this.name = subject.name;
        this.description = subject.description;
        this.subCategories = subject.subCategories;
        this.products = [];
        this.isSub = subject.isSub;
        this.containSub = subject.containSub;
        this.productsList = subject.products;
        
        this.getProducts();
        this.getProductsList();
    }

    getProducts(){
        for (var index in Data.products) {
            var product = Data.products[index];
            if(product.categories == this.id){
                this.products.push(product.id);
            }
        }

        
    }

    getProductsList(){
        for (var index in this.productsList) {
            var product = Data.products[this.productsList[index]];
            this.products.push(product.id);
        }
    }

}

export {
    Category
}