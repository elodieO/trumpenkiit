/****************
 * Product View
 *  This Class manage the product view of the project
 *  This Class allows the creation of a SecondarySceneManager instance
 *  This Class bind events related to the view scene, like close the view or spread the product
 *  The update function is called when ProjectManager state = 1
 */

'use strict';
import Data from '../data/Data';
import SecondarySceneManager from '../scenes/SecondarySceneManager';
import {
    navigationManager
} from '../navigation/NavigationManager';
import {
    productManager
} from '../products/ProductManager';
import {
    categoriesManager
} from '../categories/CategoriesManager';

class ProductView {

    constructor() {
        /*
         * Properties
         *  certain properties are related to Data values, to get the good elements in the DOM
         */


        this.element = Data.views.productView;
        this.buttonClose = Data.buttons.closeButtons.productViewButtonClose;
        this.buttonSpread = Data.buttons.productViewButtonSpread;
        this.buttonSeeOnScene = Data.buttons.productViewButtonSeeOnScene;
        this.canvas = Data.canvas.secondaryCanvas;


        /*
         * Properties
         *  is page showed or not, to know the state of the view
         */
        this.showPage = false;

        /*
         * Properties
         *  those are basic informations contained in the view and that will be replaced by the good product information
         */
        this.name = "Name";
        this.reference = "Reference";
        this.image = "Image";
        this.intro = "Introduction";
        this.description = "Description";
        this.tips = "Tips";
        this.knowledge = "knowledge";
        this.related = "Related";
        this.lettre = "Lettre";
        this.lettreCatalogue = "Lettre";
        this.composition = "Composition";
        this.spreadable = "level3";
        this.scale = "Scale";

        /*
         * Properties
         *  creation of the SecondarySceneManager instance
         */

        this.sceneManager = new SecondarySceneManager(this.canvas);
        this.sceneManager.setCameraPosition(0, 0, 1000);


        this.canvas.appendChild(this.sceneManager.renderer.domElement);

        // this.formEl = document.querySelector('#productView');
        // this.revealer = new RevealFx(this.formEl);
        // this.closeCtrl = this.formEl.querySelector('.form__close');
        // this.closeCtrl.addEventListener('click', () => navigationManager.closeProductView());

        this.waiting = 0;
        this.dragWait = false;
        this.waitingPlay = true;
        this.waitValue = 500;
        this.helpDrag = document.body.querySelector('.helpDrag');

        this.bindEventListeners();


        // this.canvas= this.formEl.querySelector("#singleProductView_canvas");

    }

    bindEventListeners() {
        window.addEventListener('resize', () => this.resizeCanvas())
        this.canvas.addEventListener('mousedown', () => this.onDocumentClick(event));
        this.buttonClose.addEventListener('click', () => navigationManager.closeProductView());
        this.buttonSpread.addEventListener('click', () => this.onSpread());
        this.buttonSeeOnScene.addEventListener('click', () => this.onSeeOnScene());
        this.resizeCanvas();
    }

    /*
     * Method
     *  resizeCanvas() allows to resize the scene when window is resized. It call the sceneManager onWindowResize function. 
     */
    resizeCanvas() {
        this.canvas.style.width = '40%';
        this.canvas.style.height = '100%';

        this.canvas.width = this.canvas.offsetWidth;
        this.canvas.height = this.canvas.offsetHeight;

        this.sceneManager.onWindowResize();
    }

    onDocumentClick(){
        this.dragWait = true;
        this.waitingPlay = true;
        this.helpDrag.classList.remove('show');
        this.waiting = 0;
    }
    /*
     * Method
     *  update function is called by ProjectManger on each frame (requestAnimationFrame) IF ProjectManager.state = 1
     */
    update() {
        this.sceneManager.update();

        if(this.waitingPlay === true){
            this.waiting = this.waiting + 1;
            if(this.waiting > this.waitValue){
                if(this.dragWait === false){
                    this.helpDrag.classList.add('show');
                    this.dragWait = true;
                    this.waitingPlay = false;
                }else{
                    this.waitValue = 5000;
                    this.waitingPlay = false;
                }
                this.waiting = 0;
            }
        }
    }

    /*
     * Method
     *  this is the method called by the navigationManager when a product if clicked, to set his informations on the view
     *  first, we get all the information of ref (the product) and then, called all the methods to set those information in the good DOM element
     */
    setInfos(ref) {
        var textZone = this.element.querySelector('.informations');
        textZone.style.backgroundColor = ref.mainColor;
        this.element.style.backgroundImage = "url('" + ref.lettre + "')"
        this.name = ref.name;
        this.reference = ref.reference;
        this.image = ref.image;
        this.intro = ref.intro;
        this.category = ref.category;
        this.description = ref.description;
        this.tips = ref.tips;
        this.knowledge = ref.knowledge;
        this.related = ref.related;
        this.lettre = ref.lettre;
        this.lettreCatalogue = ref.lettreCatalogue;
        this.composition = ref.composition;
        this.spreadable = ref.spreadable;
        this.scale = ref.scale;

        this.setName();
        this.setReference();
        this.setIntro(ref.accentColor);
        this.setCategory(ref.accentColor);
        this.setDescription();
        this.setTips();
        this.setKnowledge();
        this.setRelated();
        this.setCompo();
        this.setSpreadable();

        this.setModel();

    }

    setName() {
        var name = this.element.querySelector(".object_name");
        name.innerHTML = this.name;
    }

    setReference() {
        var reference = this.element.querySelector(".object_reference");
        reference.innerHTML = this.reference;
    }

    setIntro(color) {
        var intro = this.element.querySelector(".object_intro");
        intro.innerHTML = this.intro;
        intro.style.color = color;
    }

    setCategory(color) {
        var category = this.element.querySelector(".productInfo-category");
        var categoryParent = this.element.querySelector(".singleProductView_footer-infos p");
        var categoryName = categoriesManager.getCategory(this.category);
        category.innerHTML = categoryName.name;
        categoryParent.style.color = color;

    }

    setDescription() {
        var description = this.element.querySelector(".object_description");
        description.innerHTML = this.description;
    }

    setTips() {
        var tips = this.element.querySelector(".object_tips");
        tips.innerHTML = this.tips;
    }

    setKnowledge() {
        var knowledge = this.element.querySelector(".object_knowledge");
        knowledge.innerHTML = this.knowledge;
    }

    /*
     * Method
     *  this is the method creating the related products
     *  attaching click event on the related product
     *  if a related product is called, we need to change the current product in ProductManager and reset informations of productView with the new informations
     */
    setRelated() {
        var related = this.element.querySelector(".object_related");
        related.innerHTML = "";

        for (var index in this.related) {

            var ref = this.related[index];

            var relatedBox = document.createElement('div');
            relatedBox.setAttribute('class', 'object_related_item');
            relatedBox.setAttribute('refId', this.related[index].id);

            var relatedBox2 = document.createElement('div');
            relatedBox.setAttribute('class', 'object_related_item-imageBox');
            relatedBox.setAttribute('refId', this.related[index].id);

            relatedBox2.style.backgroundImage = "url('" + Data.products[this.related[index].id].lettre_catalogue + "')";

            var relatedBoxImg = document.createElement("img");
            relatedBoxImg.setAttribute('src', Data.products[this.related[index].id].image);

            var relatedBoxName = document.createElement('p');
            relatedBoxName.innerHTML = Data.products[this.related[index].id].name;

            relatedBox2.appendChild(relatedBoxImg);
            relatedBox.appendChild(relatedBox2);
            relatedBox.appendChild(relatedBoxName);
            related.appendChild(relatedBox);
            relatedBox.addEventListener('click', event => this.changeInformations(event));
        }

    }

    setCompo() {
        var compoZone = this.element.querySelector(".singleProductView_body-compo");
        compoZone.innerHTML = "";

        for (var index in this.composition) {

            var ref = this.composition[index];

            // 						<div class="compoBox-visual">
            // 							<img src="images/pieces/vis.png" alt="">
            // 							<p>x <span>6</span></p>
            // 						</div>
            // 						<div class="compoBox-text">
            // 							<p>Vices</p>
            // 						</div>

            var compoBox = document.createElement('div');
            compoBox.setAttribute('class', 'compoBox');


            var compoBoxVisual = document.createElement('div');
            compoBoxVisual.setAttribute('class', 'compoBox-visual');

            var compoBoxText = document.createElement('div');
            compoBoxText.setAttribute('class', 'compoBox-text');


            var compoBoxVisualImg = document.createElement("img");
            compoBoxVisualImg.setAttribute('src', ref.image);

            var compoBoxVisualText = document.createElement('p');
            compoBoxVisualText.innerHTML = "x " + ref.number;

            var compoBoxTextP = document.createElement('p');
            compoBoxTextP.innerHTML = ref.intitule;

            compoBoxVisual.appendChild(compoBoxVisualImg);
            compoBoxVisual.appendChild(compoBoxVisualText);

            compoBoxText.appendChild(compoBoxTextP);

            compoBox.appendChild(compoBoxVisual);
            compoBox.appendChild(compoBoxText);

            compoZone.appendChild(compoBox);
            // relatedBox2.appendChild(relatedBoxImg);
            // relatedBox.appendChild(relatedBox2);
            // relatedBox.appendChild(relatedBoxName);
            // related.appendChild(relatedBox);
            // relatedBox.addEventListener('click', event => this.changeInformations(event));

        }
    }

    setSpreadable() {
        if (this.spreadable) {

        } else {

        }

        switch (this.spreadable) {
            case 1:
                this.buttonSpread.classList.add("show");
                this.buttonSpread.classList.remove("show2");
                this.buttonSpread.classList.add("show1");
                break;

            case 2:
                this.buttonSpread.classList.add("show");
                this.buttonSpread.classList.add("show2");
                this.buttonSpread.classList.remove("show1");
                break;

            case 3:
                this.buttonSpread.classList.remove("show");
                this.buttonSpread.classList.remove("show1");
                this.buttonSpread.classList.remove("show2");
                break;

            default:
                break;
        }
    }

    /*
     * Method
     *  this is the method called to load the model in the sceneManager
     *  there is two cases : the model as never been loaded before (first time we click on the product), or it's already loaded, so we just pass the loaded model to the sceneManager
     */
    setModel() {


        if (productManager.currentProduct.model) {
            if (productManager.currentProduct.loadedModel) {
                console.log(productManager.currentProduct.loadedModel);
                this.loadedModel = productManager.currentProduct.loadedModel;
                this.sceneManager.addToScene();
                this.sceneManager.cleanScene();
            } else {
                this.sceneManager.loadToScene(this.scale);
                this.sceneManager.cleanScene();
            }

        }

    }

    /*
     * Method
     *  this is the method called when we click on a related product, so there is a request for updating view informations and model
     */
    changeInformations(event) {
        productManager.currentProduct.spread = false;
        var id = event.target.parentElement.parentElement.attributes[1].value;
        productManager.getCurrentProduct(id);
        navigationManager.changeProductView();
    }

    show() {
        this.showPage = true;
        this.element.classList.add("show");

        // this.revealer.reveal({
        //     bgcolor: '#fff',
        //     direction: 'tb',
        //     duration: 600,
        //     onCover: function(contentEl, revealerEl) {
        //         this.formEl.classList.add('form--open');
        //         contentEl.style.opacity = 1;
        //     }.bind(this)
        // });
    }

    hide() {
        this.showPage = false;
        this.element.classList.remove("show");

        // this.formEl.classList.remove('form--open');
        // this.revealer.reveal({
        //     bgcolor: '#8f40f1',
        //     direction: 'bt',
        //     duration: 600, 
        //     onCover: function(contentEl, revealerEl) {
        //         this.formEl.classList.remove('form--open');
        //         contentEl.style.opacity = 0;
        //     }.bind(this)
        // });
    }

    /*
     * Method
     *  when the spread button is clicked, we need to spread the product or to reset it to initail state
     */
    onSpread() {
        productManager.currentProduct.onSpread(this.element);
    }

    onSeeOnScene() {
        console.log("see on scene")
        navigationManager.onSeeOnScene();
    }

}

export let productView = new ProductView();