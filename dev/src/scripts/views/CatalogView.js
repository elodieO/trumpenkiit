/****************
 * Product View
 *  This Class manage the product view of the project
 *  This Class allows the creation of a SecondarySceneManager instance
 *  This Class bind events related to the view scene, like close the view or spread the product
 *  The update function is called when ProjectManager state = 1
 */

'use strict';
import Data from '../data/Data';
import {
    navigationManager
} from '../navigation/NavigationManager';
import {
    productManager
} from '../products/ProductManager';
import {
    categoriesManager
} from '../categories/CategoriesManager';

class CatalogView {

    constructor() {
        /*
         * Properties
         *  certain properties are related to Data values, to get the good elements in the DOM
         */
        this.alreadyLoad = false;
        this.element = Data.views.catalogView;
        this.buttonClose = Data.buttons.closeButtons.catalogViewButtonClose;
        this.buttonOpen = Data.buttons.catalogButton;
        this.catalogCategories = Data.components.catalogCategories;
        this.catalogContent = Data.components.catalogContent;

        /*
         * Properties
         *  is page showed or not, to know the state of the view
         */
        this.showPage = false;

        this.bindEventListeners();
    }

    bindEventListeners() {

        this.buttonOpen.addEventListener('click', () => navigationManager.toggleCatalogView());
       
    }

    createCatalog(){
        for (let i = 0; i < categoriesManager.categories.length; i++) {
            if(categoriesManager.categories[i].id != 100){
                var bloc = this.createCatalogCategoriesBloc(categoriesManager.categories[i]);
                this.catalogCategories[0].appendChild(bloc);
            }
            
            var content = this.createCatalogContent(categoriesManager.categories[i]);
            this.catalogContent[0].appendChild(content);
        }

        

        var childs = document.querySelectorAll('.content-child');
        for (let index = 0; index < childs.length; index++) {
            childs[index].addEventListener('click', function(){
                if(event.target.parentNode.attributes[1]){
                    var id = event.target.parentNode.attributes[1].value;
                }else{
                    var id = event.target.parentNode.parentNode.attributes[1].value;
                }
                
                productManager.getCurrentProduct(id);
                navigationManager.toggleCatalogView();
                navigationManager.showProductView();
                
            });
            
        }
    }

    createCatalogContent(category){

        var content = document.createElement('div');
        var contentHead = document.createElement('div');
        var contentBody = document.createElement('div');
        var contentName = document.createElement('h3');
        var contentIntro = document.createElement('p');
        var contentBodyLeft = document.createElement('div');
        var contentBodyRight = document.createElement('div');
       
        // Construction of content bloc
        content.classList.add("content");

        if(category.id == 100){
            content.classList.add("active");
        }
        content.setAttribute('data-category', category.id);

        // Construction of content head
        contentHead.classList.add("content-head");
        var name = document.createTextNode(category.name);
        var description = document.createTextNode(category.description);
        contentName.appendChild(name);
        contentIntro.appendChild(description);
        contentHead.appendChild(contentName);
        contentHead.appendChild(contentIntro);

        // Construction of content body
        if(category.products.length <= 3){
            contentBody.classList.add("content-body");
            contentBody.classList.add("content-body1");  
            
            for (let index = 0; index < category.products.length; index++) {
            
                var contentChild = document.createElement('div');
                var contentChildImg = document.createElement('div');
                var contentImg = document.createElement('img');
                var product = productManager.getProductInfo(category.products[index]);
    
                contentChild.classList.add("content-child");
                contentChild.setAttribute('data-id', product.id);
                contentChildImg.style.backgroundImage = "url('" + product.lettreCatalogue + "')";
                
                contentImg.setAttribute('src', product.image);
                // contentChild.append(product.name);
                var contentName = document.createElement('p');
                var name = document.createTextNode(product.name);
                contentName.appendChild(name);
                
                contentChildImg.appendChild(contentImg);
                contentChild.appendChild(contentChildImg);
                contentChild.appendChild(contentName);
    
                // contentChild.addEventListener("click", function(){
                //     productManager.getCurrentProduct(product.id);
                //     navigationManager.showProductView();
                //     navigationManager.closeCatalogView();
                // });
                contentBody.appendChild(contentChild);
            }  

        }else{
            contentBody.classList.add("content-body");
            contentBody.classList.add("content-body2");

            contentBodyLeft.classList.add("content-body-left");
            contentBodyRight.classList.add("content-body-right");

            for (let index = 0; index < 1; index++) {
                
                var contentChild = document.createElement('div');
                var contentImg = document.createElement('img');
                var contentChildImg = document.createElement('div');

                var product = productManager.getProductInfo(category.products[index]);
    
                contentChild.classList.add("content-child");
                contentChild.setAttribute('data-id', product.id);
                contentChildImg.style.backgroundImage = "url('" + product.lettreCatalogue + "')";
                
                contentImg.setAttribute('src', product.image);
                // contentChild.append(product.name);

                var contentName = document.createElement('p');
                var name = document.createTextNode(product.name);
                contentName.appendChild(name);
                
                contentChildImg.appendChild(contentImg);
                contentChild.appendChild(contentChildImg);
                contentChild.appendChild(contentName);

                contentBodyLeft.appendChild(contentChild);
                
            }

            for (let index = 1; index < category.products.length; index++) {
            
                var contentChild = document.createElement('div');
                var contentImg = document.createElement('img');
                var contentChildImg = document.createElement('div');

                var product = productManager.getProductInfo(category.products[index]);
    
                contentChild.classList.add("content-child");
                contentChild.setAttribute('data-id', product.id);
                contentChildImg.style.backgroundImage = "url('" + product.lettreCatalogue + "')";
                
                contentImg.setAttribute('src', product.image);
                // contentChild.append(product.name);
                var contentName = document.createElement('p');
                var name = document.createTextNode(product.name);
                contentName.appendChild(name);

                contentChildImg.appendChild(contentImg);
                contentChild.appendChild(contentChildImg);
                contentChild.appendChild(contentName);
               
                contentBodyRight.appendChild(contentChild);
            }  

            contentBody.appendChild(contentBodyLeft); 
            contentBody.appendChild(contentBodyRight);
            
        }        

         
        content.appendChild(contentHead);
        content.appendChild(contentBody);
        return content;

		// <div class="content-body content-body2">
		// 				<div class="content-body-left">
		// 					<div class="content-child" style="background-image: url('images/lettres/lettre_melania.svg')">
		// 						<img src="images/meubles/Melania.png" alt="">
		// 					</div>
		// 				</div>
		// 				<div class="content-body-right">
		// 					<div class="content-child" style="background-image: url('images/lettres/lettre_melania.svg')">
		// 						<img src="images/meubles/Melania.png" alt="">
		// 					</div>
		// 				</div>
		// 			</div>
    }

    createCatalogCategoriesBloc(category){
        var bloc = document.createElement('div');
        var image = document.createElement('img');
        var text = document.createElement('p');
        bloc.classList.add("category");
        if(category.containSub == true){
            bloc.classList.add("category-parent");
        }
        if(category.isSub == true){
            bloc.classList.add("category-child");
        }
        bloc.setAttribute('data-category', category.id);
        image.setAttribute('src', "images/pictos/catalogue.svg");
        text.append(category.name);
        bloc.appendChild(image);
        bloc.appendChild(text);
        bloc.addEventListener("click", () => categoriesManager.switchCategories(event));
        return bloc;
    }

    show() {
        this.showPage = true;
        this.element.classList.add("show");
    }

    hide() {
        this.showPage = false;
        this.element.classList.remove("show");
    }

}

export let catalogView = new CatalogView();