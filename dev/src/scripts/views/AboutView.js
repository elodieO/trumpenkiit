/****************
 * About View
 */

'use strict';
import Data from '../data/Data';
import {
    navigationManager
} from '../navigation/NavigationManager';

class AboutView {

    constructor() {
        /*
         * Properties
         *  certain properties are related to Data values, to get the good elements in the DOM
         */
        this.element = Data.views.aboutView;
        this.buttonClose = Data.buttons.closeButtons.aboutViewButtonClose;
        this.buttonOpen = Data.buttons.aboutButton;

        /*
         * Properties
         *  is page showed or not, to know the state of the view
         */
        this.showPage = false;

        this.bindEventListeners();
    }

    bindEventListeners() {

        this.buttonOpen.addEventListener('click', () => navigationManager.showAboutView());
        this.buttonClose.addEventListener('click', () => navigationManager.closeAboutView());
    }

    show() {
        this.showPage = true;
        this.element.classList.add("show");
        var cache = document.body.querySelector('.cache');
        cache.classList.add('show');
    }

    hide() {
        this.showPage = false;
        this.element.classList.remove("show");
        var cache = document.body.querySelector('.cache');
        cache.classList.remove('show');
    }

}

export let aboutView = new AboutView();