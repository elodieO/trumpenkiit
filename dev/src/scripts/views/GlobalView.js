/****************
 * Global View
 *  This Class manage the global view of the project
 *  This Class allows the creation of a MainSceneManager instance
 *  This Class bind events related to the view scene, like hover a product or click on it
 *  The update function is called when ProjectManager state = 0
 */
'use strict';
import Data from '../data/Data';
import MainSceneManager from '../scenes/MainSceneManager';
import Stats from '@jordandelcros/stats-js';

class GlobalView {

    constructor() {
        /*
         * Properties
         *  this.canvas called Data and his mainCanvas value, to create the THREEJS scene in it.
         */
        this.canvas = Data.canvas.mainCanvas;

        /*
         * Properties
         *  creation of the MainSceneManager instance
         */
        this.sceneManager = new MainSceneManager(this.canvas);
        this.sceneManager.setCameraPosition(350, 200, 2000);
        // this.sceneManager.setCameraPosition(350, 200, 3000);
        this.canvas.appendChild(this.sceneManager.renderer.domElement);

        this.waiting = 0;
        this.scrollWait = false;
        this.clickWait = false;
        this.waitingPlay = true;
        this.waitValue = 500;
        this.helpScroll = document.body.querySelector('.helpScroll');
        this.helpClick = document.body.querySelector('.helpClick');

        this.bindEventListeners();
    }

    bindEventListeners() {
        window.addEventListener('resize', () => this.resizeCanvas());
        window.addEventListener('keydown', event => this.onKeydown(event));
        window.addEventListener('keyup', event => this.onKeyup(event));
        this.canvas.addEventListener('wheel', event => this.onWheel(event));
        this.canvas.addEventListener('mousemove', event => this.onDocumentMouseMove(event));
        this.canvas.addEventListener('mousedown', () => this.onDocumentClick(event));
        this.canvas.addEventListener('touchstart', () => this.onDocumentClick(event));
        this.canvas.addEventListener('mouseup', () => this.onMouseUp());
        this.canvas.addEventListener('mouseover', () => this.onMouseOver(event));
        this.canvas.addEventListener('mouseout', () => this.onMouseOut(event));
        this.resizeCanvas();
    }

    /*
     * Method
     *  resizeCanvas() allows to resize the scene when window is resized. It call the sceneManager onWindowResize function. 
     */
    resizeCanvas() {
        this.canvas.style.width = '100%';
        this.canvas.style.height = '100%';

        this.canvas.width = this.canvas.offsetWidth;
        this.canvas.height = this.canvas.offsetHeight;

        this.sceneManager.onWindowResize();
    }

    /*
     * Method
     *  when mouseMove on canvas, it call the sceneManger onMouseMove function
     */
    onDocumentMouseMove(event) {
        this.sceneManager.onMouseMove(event);
        
    }

    /*
     * Method
     *  when click on canvas, it call the sceneManger onClick function
     */
    onDocumentClick(event) {
        this.sceneManager.onClick(event);
        this.scrollWait = true;
        this.waitingPlay = true;
        this.helpScroll.classList.remove('show');
        this.helpClick.classList.remove('show');
        this.waiting = 0;
    }

    onMouseUp(){
        this.sceneManager.onMouseUp();
    }

    onMouseOver(event) {
        this.sceneManager.onMouseOver(event);
    }

    onMouseOut(event) {
        this.sceneManager.onMouseOut(event);
    }

    /*
     * Method
     *  when Key down on keyboard, it calls the sceneManger onKeydown function
     */
    onKeydown(event) {
        this.sceneManager.onKeydown(event.keyCode);
        this.scrollWait = true;
        this.waitingPlay = true;
        this.helpScroll.classList.remove('show');
        this.helpClick.classList.remove('show');
        this.waiting = 0;
    }

    /*
     * Method
     *  when Key down on keyboard, it calls the sceneManger onkeyup function
     */
    onKeyup(event) {
        this.sceneManager.onKeyup(event.keyCode);
        this.scrollWait = true;
        this.waitingPlay = true;
        this.helpScroll.classList.remove('show');
        this.helpClick.classList.remove('show');
        this.waiting = 0;
    }

    /*
     * Method
     *  when mouse's wheel is used, it calls the sceneManger onScroll function
     */
    onWheel(event) {
        this.sceneManager.onWheel(event);
        this.scrollWait = true;
        this.waitingPlay = true;
        this.helpScroll.classList.remove('show');
        this.helpClick.classList.remove('show');
        this.waiting = 0;
    }

    /*
     * Method
     *  update function is called by ProjectManger on each frame (requestAnimationFrame) IF ProjectManager.state = 0
     */
    update() {
        this.sceneManager.update();

        if(this.waitingPlay === true){
            this.waiting = this.waiting + 1;
            if(this.waiting > this.waitValue){
                console.log("do something");
                if(this.scrollWait === false){
                    console.log("scroll stuff");
                    this.helpScroll.classList.add('show');
                    this.scrollWait = true;
                    this.waitingPlay = false;
                }else{
                    console.log("click stuff");
                    this.helpClick.classList.add('show');
                    this.waitValue = 5000;
                    this.waitingPlay = false;
                    this.waitingPlay = false;
                }
                this.waiting = 0;
            }
        }
        
    }

}

export let globalView = new GlobalView();