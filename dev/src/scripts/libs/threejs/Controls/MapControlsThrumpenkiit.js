/**
 * @author qiao / https://github.com/qiao
 * @author mrdoob / http://mrdoob.com
 * @author alteredq / http://alteredqualia.com/
 * @author WestLangley / http://github.com/WestLangley
 * @author erich666 / http://erichaines.com
 * @author moroine / https://github.com/moroine
 * @author chlo
 */


THREE.MapControlsThrumpenkiit = function (object, domElement) {

    this.object = object;

    this.domElement = (domElement !== undefined) ? domElement : document;

    // Set to false to disable this control
    this.enabled = true;

    // "target" sets the location of focus, where the object orbits around
    this.target = new THREE.Vector3();

    this.minLimit = -Infinity;
    this.maxLimit = Infinity;

    // Set to true to enable damping (inertia)
    // If damping is enabled, you must call controls.update() in your animation loop
    this.enableDamping = false;
    this.dampingFactor = 0.25;

    // Set to false to disable panning
    this.enablePan = true;
    this.panSpeed = 1.0;
    this.screenSpacePanning = false; // if true, pan in screen-space

    // Mouse buttons
    this.mouseButtons = {LEFT: THREE.MOUSE.LEFT, MIDDLE: THREE.MOUSE.MIDDLE, RIGHT: THREE.MOUSE.RIGHT};

    // for reset
    this.target0 = this.target.clone();
    this.position0 = this.object.position.clone();


    /**********
     * public methods
     ***********/

    this.reset = function () {

        scope.target.copy(scope.target0);
        scope.object.position.copy(scope.position0);

        scope.object.updateProjectionMatrix();
        scope.dispatchEvent(changeEvent);

        scope.update();

        state = STATE.NONE;

    };

    this.update = function () {

        var offset = new THREE.Vector3();
        var lastPosition = new THREE.Vector3();

        return function update() {

            var position = scope.object.position;
            offset.copy(position).sub(scope.target);

            // move target to panned location
            scope.target.add(panOffset);

            position.copy(scope.target).add(offset);

            // sphericalDelta.theta *= (1 - scope.dampingFactor);
            // sphericalDelta.phi *= (1 - scope.dampingFactor);
            panOffset.multiplyScalar(1 - scope.dampingFactor);

            if (lastPosition.distanceToSquared(scope.object.position) > EPS) {

                scope.dispatchEvent(changeEvent);
                lastPosition.copy(scope.object.position);
                return true;
            }

            return false;

        };

    }( );

    this.dispose = function () {
        scope.domElement.removeEventListener('mousedown', onMouseDown, false);

        scope.domElement.removeEventListener('touchstart', onTouchStart, false);
        scope.domElement.removeEventListener('touchend', onTouchEnd, false);
        scope.domElement.removeEventListener('touchmove', onTouchMove, false);

        document.removeEventListener('mousemove', onMouseMove, false);
        document.removeEventListener('mouseup', onMouseUp, false);

    };


    /**********
     * internals
     ***********/

    var scope = this;

    var changeEvent = {type: 'change'};
    var startEvent = {type: 'start'};
    var endEvent = {type: 'end'};

    var STATE = {
        NONE: 0,
        PAN: 1,
    };

    var state = STATE.NONE;

    var EPS = 0.000001;

    var sphericalDelta = new THREE.Spherical();
    
    var panOffset = new THREE.Vector3();
    
    var panStart = new THREE.Vector2();
    var panEnd = new THREE.Vector2();
    var panDelta = new THREE.Vector2();

    var panLeft = function () {

        var v = new THREE.Vector3();

        return function panLeft(distance, objectMatrix) {
            v.setFromMatrixColumn(objectMatrix, 0); // get X column of objectMatrix
            v.multiplyScalar(-distance);

            // console.log(scope.object.position.x);

            // if (v.x === -v.x){
            //     console.log(v.x);
            // }

            // if (scope.object.position.x < 4000 && scope.object.position.x > 250){
            // panOffset.add(v);
            //  }

            if (!(v.x < 0 && scope.object.position.x < scope.minLimit) && !(v.x > 0 && scope.object.position.x > scope.maxLimit)){
               panOffset.add(v);
            }
        };
    }();

    // deltaX and deltaY are in pixels; right and down are positive
    var pan = function () {

        var offset = new THREE.Vector3();

        return function pan(deltaX) {

            var element = scope.domElement === document ? scope.domElement.body : scope.domElement;

            if (scope.object.isPerspectiveCamera) {
                // perspective
                var position = scope.object.position;
                offset.copy(position).sub(scope.target);
                var targetDistance = offset.length();

                // half of the fov is center to top of screen
                targetDistance *= Math.tan((scope.object.fov / 2) * Math.PI / 180.0);

                // we use only clientHeight here so aspect ratio does not distort speed
                panLeft(2 * deltaX * targetDistance / element.clientHeight, scope.object.matrix);

            } else if (scope.object.isOrthographicCamera) {
                // orthographic
                panLeft(deltaX * (scope.object.right - scope.object.left) / scope.object.zoom / element.clientWidth, scope.object.matrix);

            } else {
                // camera neither orthographic nor perspective
                console.warn('WARNING: MapControlsThrumpenkiit.js encountered an unknown camera type - pan disabled.');
                scope.enablePan = false;
            }
        };

    }();


    /*********
     * event callbacks - update the object state
     **********/

    function handleMouseDownPan(event) {
        //console.log( 'handleMouseDownPan' );
        panStart.set(event.clientX, event.clientY);
    }

    function handleMouseMovePan(event) {
        panEnd.set(event.clientX, event.clientY);
        panDelta.subVectors(panEnd, panStart).multiplyScalar(scope.panSpeed);
        pan(panDelta.x);
        panStart.copy(panEnd);
        scope.update();
    }

    function handleMouseUp(event) {
        // console.log( 'handleMouseUp' );
    }

    function handleTouchStartPan(event) {
        if (scope.enablePan) {
            // console.log( 'handleTouchStartPan' );
            panStart.set(event.touches[0].pageX, event.touches[0].pageY);
        }
    }

    function handleTouchMovePan(event) {

        if (scope.enablePan === false) return;
        if ((state & STATE.PAN) === 0) return;
        // console.log( 'handleTouchMovePan' );

        panEnd.set(event.touches[0].pageX, event.touches[0].pageY);
        panDelta.subVectors(panEnd, panStart).multiplyScalar(scope.panSpeed);
        pan(panDelta.x);
        panStart.copy(panEnd);
    }

    function handleTouchEnd(event) {
        //console.log( 'handleTouchEnd' );
    }


    /**********
     /* event handlers - FSM: listen for events and reset state
     ***********/

    function onMouseDown(event) {

        if (scope.enabled === false) return;

        event.preventDefault();

        switch (event.button) {
            case scope.mouseButtons.LEFT:
                if (scope.enablePan === false) return;
                handleMouseDownPan(event);
                state = STATE.PAN;
                break;
        }

        if (state !== STATE.NONE) {

            document.addEventListener('mousemove', onMouseMove, false);
            document.addEventListener('mouseup', onMouseUp, false);

            scope.dispatchEvent(startEvent);
        }
    }

    function onMouseMove(event) {

        if (scope.enabled === false) return;

        event.preventDefault();

        switch (state) {
            case STATE.PAN:
                if (scope.enablePan === false) return;
                handleMouseMovePan(event);
                break;
        }
    }

    function onMouseUp(event) {

        if (scope.enabled === false) return;

        handleMouseUp(event);

        document.removeEventListener('mousemove', onMouseMove, false);
        document.removeEventListener('mouseup', onMouseUp, false);

        scope.dispatchEvent(endEvent);

        state = STATE.NONE;
    }

    function onTouchStart(event) {

        if (scope.enabled === false) return;

        event.preventDefault();

        switch (event.touches.length) {
            case 1:	// one-fingered touch: pan
                if (scope.enablePan === false) return;
                handleTouchStartPan(event);
                state = STATE.PAN;
                break;
            default:
                state = STATE.NONE;
        }

        if (state !== STATE.NONE) {
            scope.dispatchEvent(startEvent);
        }
    }

    function onTouchMove(event) {

        if (scope.enabled === false) return;

        event.preventDefault();
        event.stopPropagation();

        switch (event.touches.length) {

            case 1: // one-fingered touch: pan
                if (scope.enablePan === false) return;
                if (state !== STATE.PAN) return;
                handleTouchMovePan(event);
                scope.update();
                break;

            default:
                state = STATE.NONE;
        }
    }

    function onTouchEnd(event) {
        if (scope.enabled === false) return;
        handleTouchEnd(event);
        scope.dispatchEvent(endEvent);
        state = STATE.NONE;
    }

    scope.domElement.addEventListener('mousedown', onMouseDown, false);

    scope.domElement.addEventListener('touchstart', onTouchStart, false);
    scope.domElement.addEventListener('touchend', onTouchEnd, false);
    scope.domElement.addEventListener('touchmove', onTouchMove, false);

    // force an update at start
    this.update();

};

THREE.MapControlsThrumpenkiit.prototype = Object.create(THREE.EventDispatcher.prototype);
THREE.MapControlsThrumpenkiit.prototype.constructor = THREE.MapControlsThrumpenkiit;

Object.defineProperties(THREE.MapControlsThrumpenkiit.prototype, {

    center: {

        get: function () {

            console.warn('THREE.MapControlsThrumpenkiit: .center has been renamed to .target');
            return this.target;

        }

    },

    // backward compatibility
    noPan: {

        get: function () {

            console.warn('THREE.MapControlsThrumpenkiit: .noPan has been deprecated. Use .enablePan instead.');
            return !this.enablePan;

        },

        set: function (value) {

            console.warn('THREE.MapControlsThrumpenkiit: .noPan has been deprecated. Use .enablePan instead.');
            this.enablePan = !value;

        }

    },

    noKeys: {

        get: function () {

            console.warn('THREE.MapControlsThrumpenkiit: .noKeys has been deprecated. Use .enableKeys instead.');
            return !this.enableKeys;

        },

        set: function (value) {

            console.warn('THREE.MapControlsThrumpenkiit: .noKeys has been deprecated. Use .enableKeys instead.');
            this.enableKeys = !value;

        }

    },

    staticMoving: {

        get: function () {

            console.warn('THREE.MapControlsThrumpenkiit: .staticMoving has been deprecated. Use .enableDamping instead.');
            return !this.enableDamping;

        },

        set: function (value) {

            console.warn('THREE.MapControlsThrumpenkiit: .staticMoving has been deprecated. Use .enableDamping instead.');
            this.enableDamping = !value;

        }

    },

    dynamicDampingFactor: {

        get: function () {

            console.warn('THREE.MapControlsThrumpenkiit: .dynamicDampingFactor has been renamed. Use .dampingFactor instead.');
            return this.dampingFactor;

        },

        set: function (value) {

            console.warn('THREE.MapControlsThrumpenkiit: .dynamicDampingFactor has been renamed. Use .dampingFactor instead.');
            this.dampingFactor = value;

        }

    }

});
