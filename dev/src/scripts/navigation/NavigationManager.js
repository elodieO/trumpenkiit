/****************
 * Navigation Manager
 *  This manager manage the navigation between the different views 
 *  navigationManager is a Singleton of NavigationManager accesible from all other Classes
 */

'use strict';

import {
    productView
} from '../views/ProductView';
import {
    productManager
} from '../products/ProductManager';
import {
    bubbleComponent
} from '../components/BubbleComponent';
import {
    catalogView
} from '../views/CatalogView';
import {
    aboutView
} from '../views/AboutView';
import {
    helpView
} from '../views/HelpView';
import {
    globalView
} from '../views/GlobalView';
import Data from '../data/Data';

class NavigationManager {

    constructor() {

        /*
         * State properties
         *  This state is usefull for ProjectManager update method
         */
        this.state = 0;
    }

    /*
     * Show Product View function
     *  This function open the productView and set the informations in it
     */
    showProductView() {

        productView.setInfos(productManager.currentProduct);

        if (productView.showPage == false) {
            productView.show();
            bubbleComponent.hide();
            this.state = 1;
        }

        if(catalogView.showPage == true){
            catalogView.hide();
        }

    }

    /*
     * Change Product View function
     *  This function update productView informations
     */
    changeProductView() {
        productView.setInfos(productManager.currentProduct);
    }

    /*
     * Close Product View function
     *  This function close productView
     */
    closeProductView() {
        productView.hide();
        this.state = 0;
    }

    /*
     * Show Product View function
     *  This function open the productView and set the informations in it
     */
    showCatalogView() {

        if(catalogView.alreadyLoad == false){
            catalogView.createCatalog();
            catalogView.alreadyLoad = true;
        }
        catalogView.show();
        bubbleComponent.hide();
        this.state = 2;
        var buttonCatalog = Data.buttons.catalogButton;
        buttonCatalog.classList.add('catalogShow');

    }

    closeCatalogView(){
        catalogView.hide();
        if(productView.showPage == true){
            this.state = 1;
        }else{
            this.state = 0;
        }
        var buttonCatalog = Data.buttons.catalogButton;
        buttonCatalog.classList.remove('catalogShow');
        
    }

    toggleCatalogView(){
        if(this.state == 2){
            var buttonCatalog = Data.buttons.catalogButton;
            buttonCatalog.classList.add('catalogShow');
            catalogView.hide();
            if(productView.showPage == true){
                this.state = 1;
            }else{
                this.state = 0;
            }
        }else{
            var buttonCatalog = Data.buttons.catalogButton;
            buttonCatalog.classList.remove('catalogShow');
            if(catalogView.alreadyLoad == false){
                catalogView.createCatalog();
                catalogView.alreadyLoad = true;
            }
            catalogView.show();
            bubbleComponent.hide();
            this.state = 2;
        }
    }

    onSeeOnScene(){
        
        var current = productManager.currentProduct;
        var productsInScene = globalView.sceneManager.sceneProducts;
        console.log(current, productsInScene);
        for (let index = 0; index < productsInScene.length; index++) {
           
            if(current.id == productsInScene[index].myID){
                console.log(productsInScene[index]);
                var vector = new THREE.Vector3();
                productsInScene[index].getWorldPosition(vector);
                console.log(vector);
                globalView.sceneManager.moveCamera(vector);
                this.closeProductView();

            }
            
        }
    }

    showAboutView() {

        aboutView.show();
        bubbleComponent.hide();
        this.state = 3;
      
    }

    closeAboutView(){
        aboutView.hide();
       
        if(productView.showPage == true){
            this.state = 1;
        }else if(catalogView.showPage == true){
            this.state = 2;
        }else{
            this.state = 0;
        }
        
    }


    showHelpView() {

       
        if(productView.showPage == true){
            helpView.element.classList.add('state1');
            helpView.element.classList.remove('state0');
        }else{
            helpView.element.classList.remove('state1');
            helpView.element.classList.add('state0');
        }
        helpView.show();
        bubbleComponent.hide();
        this.state = 4;

    }

    closeHelpView(){
        helpView.hide();
        if(productView.showPage == true){
            this.state = 1;
        }else{
            this.state = 0;
        }
    }

}

export let navigationManager = new NavigationManager();